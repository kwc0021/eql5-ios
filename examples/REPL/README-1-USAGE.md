### Quicklisp

To load Quicklisp (included), simply eval:

```
  (eql:quicklisp)
```
(or just `:q` on the command line)

Please note that because of the limited stack size on iOS, some libraries
might not install/work.



### Included ASDF libraries

The following libraries are already included, ready to use:
`:cl-ppcre`, `:iterate`, `:split-sequence`.



### Entering parens

Of course we need an easy way to enter `(` and `)`, independent from the
keyboard. Above the keyboard you find 2 buttons for this purpose. Holding the
`)` button will close all currently open parens.



### Copy, Paste, Eval one expression

(works both in the editor and the command line)

Tap and hold on the beginning (or inside) of an expression (e.g. on `defun`) to
select the whole expression, and to show the popup menu for copy and paste. If
you paste the expression to a different indentation level, the indentation will
be adapted automatically.

To eval the selected expression only, just click on the lambda button of the
popup menu.



### Copy result to global Clipboard

In order to use the result in another app, just enter `*` followed by RET, as
you would do to show the latest eval value.

This will copy the result to the clipboard.



### Moving the cursor

With the arrow buttons on the right above the keyboard you can move the cursor
in both the editor and the command line (where up and down will move it to the
beginning and end, respectively).

Tap-and-hold for moving to beginning/end of line (left/right) or file (up/down).



### Auto completion

Entering 2 spaces within half a second will start auto completion. It will
complete only a part, if ambiguous.

On abbreviations, like `m-v-b` for `multiple-value-bind`, it will complete the
whole word, preferring the first alphabetical match if ambiguous.

Please note that auto completion only works for a fixed set of CL symbols (and
**not** for user defined functions or variables).



### Search

Use `:?` on the command line to find a **regular expression** (or plain text)
in the editor:

```
  :? prin[1c] ; finds both PRIN1 and PRINC

  :? "hello"
```
To find the next match, just hit the [Return] button. The search will restart on
top after the last match.

If you only need to search for **plain text** (so you don't need to escape
certain characters), you can put this in your `.eclrc`:

```
  (setf editor:*plain-text-search* t)
```



### External keyboard

* keys **Up** and **Down** move in command line history
* key **Tab** switches focus between the edtior and the command line
* keys **Alt+E** (E for **E**xpression) selects s-expression (like tap-and-hold)
* keys **Alt+L** (L for **L**ambda) evals selected (single) expression



### Loading remote files into the editor

If you are connected to a local WiFi, run script `web-server.sh` (see sources)
on your desktop computer, in the directory where your Lisp file is located.
Now load the file like this:
```
  (load* "example" t) ; pass T to load it into the editor
```
You will be prompted for the last number of your desktop computer's local IP
address.



### Tips

If you want to print some values while your code is running, just use function
```
  (ed:pr x) ; short for (editor:append-output x), see function definition
```
The argument x can be of any type and will be printed immediately (think of a
long running loop).
You may also define the text color etc. (see function definition).

You can see your command history by simply opening file
`.eql5-lisp-repl-history` in your home directory.

When you enter a new file name for saving a file, you may add a path with new
directories: they will be created before saving the file.

If the output window has too much contents (thousands of lines, may happen
accidentally), and is becoming slow, remember that you're using the same Lisp
image where your REPL lives in, so you can clear it directly like this:

```
  (in-package :editor)

  (q! |clear| *qml-output*)
```
(or just `:c` on the command line)

For trivial debugging (or simple output) you can use a (blocking) **message
box**; it accepts any Lisp value, converting it to a string if necessary.

```
  (eql:qmsg 42) ; returns its argument, just like PRINT
```



### Slime

Please see [README-SLIME](README-3-SLIME.md)
