Info
----

This example uses its own copy/paste menu, and there is no official way to
disable the iOS menu within QML. Since this would get in our way, we simply use
a small hack to make it possible to disable the menu from Lisp like so:

  (eql:disable-clipboard-menu) ; defined in 'lisp/ini.lisp'


HowTo
-----

* install Qt5.13 and select to install the sources
* copy 'qiostextinputoverlay.mm' from this directory to the path in 'path.txt'
* switch to the above path and open 'ios.pro' in Qt Creator
* select the iOS build / release version, choose the 'ios' directory for output
* build the lib from within Qt Creator
* close Qt Creator and run 'make install'

Now you have a Qt version that allows disabling the clipboard menu.
