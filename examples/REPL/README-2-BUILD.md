
Preamble
--------

Since this example needs a small hack to the Qt sources to work properly,
please see [qt-ios-hack](qt-ios-hack/) (should be easy to follow).


Info
----

This is a port of the EQL5-Android 'CL REPL' example.

Please see example [../my](../my) for building/deploying the app or running it
on the desktop (the basics are the same). Note that running this example on the
desktop works only from within Slime.

Because of some limitations and workarounds needed for iOS, this can't really
be used on the desktop (it would need some adaption for desktop only, undoing
the workarounds for iOS).

Additionally, the usual `eql5 run.lisp` won't work on the desktop with this
example, you would need to first run `eql5`, followed by `(load "run")`.
