(in-package :editor)

(defparameter *code-font* (qnew "QFont(QString,int)"
                                #+darwin  "Monaco"      #+darwin  12
                                #+linux   "Monospace"   #+linux   15
                                #+windows "Courier New" #+windows 10))

;;(q> |font| *qml-edit* *code-font*)
;;(q> |font| *qml-command* *code-font*)
;;(q> |fontSize| *qml-output-view* (|pixelSize| *code-font*))

