;;; THIS FILE IS GENERATED, see '(eql:qml)'

(defpackage ui
  (:use :cl :eql)
  (:export
   #:*buttons-right*
   #:*buttons-top*
   #:*clear*
   #:*clipboard-menu*
   #:*command*
   #:*copy*
   #:*cut*
   #:*debug-dialog*
   #:*debug-input*
   #:*debug-model*
   #:*debug-text*
   #:*down*
   #:*edit*
   #:*eval*
   #:*eval-exp*
   #:*file-browser*
   #:*file-edit*
   #:*flick-command*
   #:*flick-edit*
   #:*folder-model*
   #:*folder-view*
   #:*font-bigger*
   #:*font-smaller*
   #:*hide-buttons-right*
   #:*hide-buttons-top*
   #:*history-back*
   #:*history-forward*
   #:*left*
   #:*main*
   #:*open-file*
   #:*output*
   #:*output-model*
   #:*paste*
   #:*path*
   #:*progress*
   #:*query-dialog*
   #:*query-input*
   #:*query-text*
   #:*rect-arrows*
   #:*rect-command*
   #:*rect-edit*
   #:*rect-output*
   #:*rect-paren-buttons*
   #:*redo*
   #:*right*
   #:*save-file*
   #:*select-all*
   #:*show-buttons-right*
   #:*show-buttons-top*
   #:*show-menu*
   #:*undo*
   #:*up*))

(provide :ui-vars)

(in-package :ui)

(defparameter *clear*              "clear")              ; Button          "qml/repl.qml"
(defparameter *copy*               "copy")               ; Button          "qml/ext/ClipboardMenu.qml"
(defparameter *cut*                "cut")                ; Button          "qml/ext/ClipboardMenu.qml"
(defparameter *down*               "down")               ; Button          "qml/repl.qml"
(defparameter *eval*               "eval")               ; Button          "qml/repl.qml"
(defparameter *eval-exp*           "eval_exp")           ; Button          "qml/ext/ClipboardMenu.qml"
(defparameter *file-edit*          "file_edit")          ; Button          "qml/ext/FileBrowser.qml"
(defparameter *font-bigger*        "font_bigger")        ; Button          "qml/repl.qml"
(defparameter *font-smaller*       "font_smaller")       ; Button          "qml/repl.qml"
(defparameter *history-back*       "history_back")       ; Button          "qml/repl.qml"
(defparameter *history-forward*    "history_forward")    ; Button          "qml/repl.qml"
(defparameter *left*               "left")               ; Button          "qml/repl.qml"
(defparameter *open-file*          "open_file")          ; Button          "qml/repl.qml"
(defparameter *paste*              "paste")              ; Button          "qml/ext/ClipboardMenu.qml"
(defparameter *redo*               "redo")               ; Button          "qml/repl.qml"
(defparameter *right*              "right")              ; Button          "qml/repl.qml"
(defparameter *save-file*          "save_file")          ; Button          "qml/repl.qml"
(defparameter *select-all*         "select_all")         ; Button          "qml/ext/ClipboardMenu.qml"
(defparameter *show-menu*          "show_menu")          ; Button          "qml/repl.qml"
(defparameter *undo*               "undo")               ; Button          "qml/repl.qml"
(defparameter *up*                 "up")                 ; Button          "qml/repl.qml"
(defparameter *file-browser*       "file_browser")       ; FileBrowser     "qml/ext/FileBrowser.qml"
(defparameter *flick-command*      "flick_command")      ; Flickable       "qml/repl.qml"
(defparameter *flick-edit*         "flick_edit")         ; Flickable       "qml/repl.qml"
(defparameter *folder-model*       "folder_model")       ; FolderListModel "qml/ext/FileBrowser.qml"
(defparameter *debug-model*        "debug_model")        ; ListModel       "qml/ext/DebugDialog.qml"
(defparameter *output-model*       "output_model")       ; ListModel       "qml/repl.qml"
(defparameter *debug-text*         "debug_text")         ; ListView        "qml/ext/DebugDialog.qml"
(defparameter *folder-view*        "folder_view")        ; ListView        "qml/ext/FileBrowser.qml"
(defparameter *output*             "output")             ; ListView        "qml/repl.qml"
(defparameter *hide-buttons-right* "hide_buttons_right") ; NumberAnimation "qml/repl.qml"
(defparameter *hide-buttons-top*   "hide_buttons_top")   ; NumberAnimation "qml/repl.qml"
(defparameter *show-buttons-right* "show_buttons_right") ; NumberAnimation "qml/repl.qml"
(defparameter *show-buttons-top*   "show_buttons_top")   ; NumberAnimation "qml/repl.qml"
(defparameter *clipboard-menu*     "clipboard_menu")     ; Popup           "qml/ext/ClipboardMenu.qml"
(defparameter *progress*           "progress")           ; ProgressBar     "qml/repl.qml"
(defparameter *buttons-right*      "buttons_right")      ; Rectangle       "qml/repl.qml"
(defparameter *buttons-top*        "buttons_top")        ; Rectangle       "qml/repl.qml"
(defparameter *debug-dialog*       "debug_dialog")       ; Rectangle       "qml/ext/DebugDialog.qml"
(defparameter *query-dialog*       "query_dialog")       ; Rectangle       "qml/ext/QueryDialog.qml"
(defparameter *rect-arrows*        "rect_arrows")        ; Rectangle       "qml/repl.qml"
(defparameter *rect-command*       "rect_command")       ; Rectangle       "qml/repl.qml"
(defparameter *rect-edit*          "rect_edit")          ; Rectangle       "qml/repl.qml"
(defparameter *rect-output*        "rect_output")        ; Rectangle       "qml/repl.qml"
(defparameter *rect-paren-buttons* "rect_paren_buttons") ; Rectangle       "qml/repl.qml"
(defparameter *main*               "main")               ; StackView       "qml/repl.qml"
(defparameter *query-text*         "query_text")         ; Text            "qml/ext/QueryDialog.qml"
(defparameter *command*            "command")            ; TextEdit        "qml/repl.qml"
(defparameter *edit*               "edit")               ; TextEdit        "qml/repl.qml"
(defparameter *debug-input*        "debug_input")        ; TextField       "qml/ext/DebugDialog.qml"
(defparameter *path*               "path")               ; TextField       "qml/ext/FileBrowser.qml"
(defparameter *query-input*        "query_input")        ; TextField       "qml/ext/QueryDialog.qml"
