(defpackage :dialogs
  (:use :cl :eql :qml)
  (:export
   #:query-dialog
   #:debug-dialog
   #:get-file-name
   #:exited
   #:push-dialog
   #:pop-dialog
   #:*file-name*))

(in-package :dialogs)

(defvar *file-name* nil)
(defvar *callback*  nil)

;; '(js ui:*main* ...)': see JS functions in '../qml/repl.qml'

(defun push-dialog (name)
  (disable-clipboard-menu nil)
  (js ui:*main* "push~ADialog()" (string-capitalize name)))

(defun pop-dialog ()
  "Pops the currently shown dialog, returning T if there was a dialog to pop."
  (disable-clipboard-menu)
  (prog1
      (> (q< |depth| ui:*main*) 1)
    (js ui:*main* "popDialog()")
    (exited)))

(defun wait-while-transition ()
  ;; needed for evtl. recursive calls
  (x:while (q< |busy| ui:*main*)
    (qsleep 0.1)))

(defun query-dialog (query)
  (unless (x:empty-string query)
    (q> |text| ui:*query-text* (string-trim '(#\Newline) query)))
  (q! |clear| ui:*query-input*)
  (wait-while-transition)
  (push-dialog :query)
  (q! |forceActiveFocus| ui:*query-input*)
  (|show| (|inputMethod.QGuiApplication|)) ; needed on recursive calls
  (wait-for-closed)
  (pop-dialog)
  (qlater (lambda () (editor:ensure-focus :show)))
  (q< |text| ui:*query-input*))

(defun append-debug-output (text color bold)
  (qjs |addBlock| ui:*debug-model*
       text color bold))

(defun debug-dialog (messages)
  (q! |clear| ui:*debug-model*)
  (q> |text| ui:*debug-input* ":q")
  (dolist (text/color messages)
    (let* ((text (string-trim '(#\Newline) (car text/color)))
           (color (cdr text/color))
           (bold (not (string= "black" color)))) ; boolean
      (append-debug-output text color bold)))
  (wait-while-transition)
  (push-dialog :debug)
  (q! |forceActiveFocus| ui:*debug-input*)
  (qsingle-shot 500 (lambda () (q! |positionViewAtEnd| ui:*debug-text*)))
  (wait-for-closed)
  (pop-dialog)
  (qlater (lambda () (editor:ensure-focus :show)))
  (q< |text| ui:*debug-input*))

(let ((exited t))
  (defun wait-for-closed ()
    (setf exited nil)
    (x:while (not  exited) ; don't suspend the thread (busy waiting is more predictable)
      (qsleep 0.1)))
  (defun exited ()         ; called from QML
    (setf exited t)))

;; file browser

(let ((1st t))
  (defun get-file-name (&optional callback focus)
    (|hide| (|inputMethod.QGuiApplication|))
    (when 1st
      (setf 1st nil)
      (set-file-browser-path ":data"))
    (setf *callback* callback)
    ;; force update
    (qlet ((none "QUrl")
           (curr (q< |folder| ui:*folder-model*)))
      (dolist (folder (list none curr))
        (q> |folder| ui:*folder-model* folder)))
    (q> |editMode| ui:*file-browser* nil)
    (push-dialog :file)
    (when focus
      (qsingle-shot 500 (lambda () (q! |forceActiveFocus| ui:*path*))))))

(defun directory-p (path)
  (qlet ((info "QFileInfo(QString)" path))
    (|isDir| info)))

(defun set-file-name (file-name) ; called from QML
  (let ((name (remove-if (lambda (ch) (find ch "*?\\")) file-name)))
    (if (directory-p name)
        (set-file-browser-path name)
        (progn
          (setf *file-name* name)
          (when *callback*
            (funcall *callback*))
          (qlater 'pop-dialog))))) ; QLATER: prevent crash (internal threads are a bitch)

(defun rename-file* (from to) ; called from QML
  (ignore-errors (rename-file from to)))


(defun location (name)
  (cond ((string= ":storage" name)
         "/")
        ((string= ":data" name)
         (namestring (probe-file (merge-pathnames "../Documents/"))))
        ((string= ":home" name)
         (namestring (user-homedir-pathname)))))

(defun set-file-browser-path (path) ; called from QML
  (let ((path* (x:cc "file://" (if (x:starts-with ":" path)
                                   (location path)
                                   path))))
    (qlet ((url "QUrl(QString)" (if (x:ends-with "/" path*)
                                    path*
                                    (x:cc path* "/"))))
      (q> |folder| ui:*folder-model* url))))
