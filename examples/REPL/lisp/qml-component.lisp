;;; dynamically load/create QML item from file, add it as parent
;;; of the root item, and show it on top of all other items

(in-package :eql)

(defvar *qml-component* nil)
(defvar *qml-instance*  nil)
(defvar *qml-file*      nil)

(defun component-instance (component)
  (qt-object-? (|create| component)))

(defun reload-qml-file ()
  (qlater 'qml-item-from-file))

(defun qml-item-from-file (&optional (file *qml-file*) (name "my"))
  (setf *qml-file* file)
  (let ((engine (|engine| qml:*quick-view*)))
    (unless *qml-component*
      (setf *qml-component* (qnew "QQmlComponent(QQmlEngine*)" engine)))
    (qdel *qml-instance*)
    (|clearComponentCache| engine)
    (|loadUrl| *qml-component* (|fromLocalFile.QUrl| file))
    (if (|isError| *qml-component*)
        (qmsg (x:join (mapcar '|toString| (|errors| *qml-component*))
                      #.(make-string 2 :initial-element #\Newline)))
        (let ((item (component-instance *qml-component*)))
          (|setObjectName| item name)
          (|setParent| item (qml:root-item))
          (|setParentItem| item (qml:root-item))
          (qml:q> |z| item 100) ; stay on top
          (setf *qml-instance* item)))))

(export 'qml-item-from-file)
