### Prepare (requires WiFi)

#### Mobile device

First make sure that your device is disconnected from USB. Since a working
(patched) Swank version is already included, just eval `(eql:start-swank)`,
or simply `:s`, to start the server. Wait until an IP address is displayed.
(The first time will take longer because of byte-compiling Swank.)

#### Mac/PC

On the Mac/PC you should preferably use Slime v2.24 (newer versions may work,
but will show a warning at startup). Just connect to the IP address displayed
on the mobile device.



### Test

To test if it works, you can try:

```
  (|pret
```

followed by `TAB`, which should complete to:

```
  (|prettyProductName.QSysInfo|)
```

This will print your OS version.

Just for fun you may try:

```
  (in-package :editor)

  (q> |text| *qml-command* "(+ 1 2)")

  (q! |append| *qml-command* (string #\Newline))
```



### Installing Quicklisp libraries

Since you can't really follow the progress while installing Quicklisp libraries
directly on the mobile device, it may be more convenient to install them
via the Slime REPL. In order to load/install Quicklisp from there, you need to
eval: `(eql:quicklisp)`



### Developing a GUI interactively, using QML

You can either use the EQL5 desktop version to build your app on the desktop
first, or you use the approach shown in example `../my`, which is fully
interactive for both Lisp and QML.

See also file `lisp/qml-lisp` for interacting with QML, especially
`(qml:reload)`. Make sure to set an `objectName` to every QML item you
want to change interactively.

