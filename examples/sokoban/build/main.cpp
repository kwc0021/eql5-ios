#undef SLOT

#include <ecl/ecl.h>
#include <eql5/eql.h>
#include <QApplication>
#include <QTextCodec>
#include <QFileInfo>
#include <QtDebug>

extern "C" void ini_app(cl_object);

QT_BEGIN_NAMESPACE

int main(int argc, char** argv) {

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    qputenv("QT_QPA_NO_TEXT_HANDLES", "1"); // no text selection handles/menu (we use our own)

    QApplication qapp(argc, argv);
    //qApp->setOrganizationName("MyTeam");
    //qApp->setOrganizationDomain("my-team.org");
    qApp->setApplicationName(QFileInfo(qApp->applicationFilePath()).baseName());

    QTextCodec* utf8 = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(utf8);

    // this was needed in past for GC on iOS
    /*
    const int size = 256;
    char* ecl_argv[size];
    ecl_argv[0] = "";
    GC_allow_register_threads();
    GC_register_my_thread((const struct GC_stack_base*)ecl_argv);
    GC_stackbottom = (char*)(ecl_argv + size - 1);
    setenv("ECLDIR", "", 1);
    */

    EQL eql;
    // fallback restart for connections from Slime
    eql.exec(ini_app, "(loop (with-simple-restart (restart-qt-events \"Restart Qt event processing.\") (qexec)))");

    return 0; }

QT_END_NAMESPACE

