xcodebuild build \
  -project sokoban.xcodeproj \
  -scheme sokoban \
  -configuration Release \
  -destination generic/platform=iOS \
  -destination-timeout 1 \
  ENABLE_ONLY_ACTIVE_RESOURCES=NO \
  ENABLE_BITCODE=NO \
  -allowProvisioningUpdates
