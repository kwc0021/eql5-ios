QT             = widgets uitools quick quickwidgets qml network
TEMPLATE       = app
CONFIG         += no_keywords release
ECL_DIR        = $$(ECL_IOS)
INCLUDEPATH    += $$ECL_DIR/include ../../include
LIBS           += -L$$ECL_DIR/lib -lecl -leclatomic -leclffi -leclgc -leclgmp
LIBS           += -L../../lib -leql5 -lini_eql5 -leql5_quick -leql5_network
LIBS           += -L./build -lapp
PRE_TARGETDEPS += build/libapp.a
TARGET         = sokoban
OBJECTS_DIR    = ./tmp/
MOC_DIR        = ./tmp/

SOURCES        += build/main.cpp

RESOURCES      = sokoban.qrc

ios {
    # comment out on very first build of any new project,
    # then copy generated Info.plist to below path
    # (run 'open Release.../Info.plist', export as xml)
    QMAKE_INFO_PLIST  = ios/Info.plist

    ios_icon.files    = $$files($$PWD/ios/icon*.png)
    QMAKE_BUNDLE_DATA += ios_icon

    ios_launch.files  = $$PWD/ios/designable.storyboard $$PWD/../../img/logo.png
    QMAKE_BUNDLE_DATA += ios_launch
}
