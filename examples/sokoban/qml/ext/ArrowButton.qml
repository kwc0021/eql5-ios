import QtQuick 2.10
import QtQuick.Controls 2.10

Button {
    width: main.isPhone ? 37 : 50
    height: width
    flat: true
    focusPolicy: Qt.NoFocus
    font.family: fontAwesome.name
    font.pixelSize: 1.2 * width
    opacity: 0.2
    scale: 1.2
}
