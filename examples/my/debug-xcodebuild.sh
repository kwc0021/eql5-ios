xcodebuild build \
  -project my.xcodeproj \
  -scheme my \
  -configuration Debug \
  -destination generic/platform=iOS \
  -destination-timeout 1 \
  ENABLE_ONLY_ACTIVE_RESOURCES=NO \
  ENABLE_BITCODE=NO \
  -allowProvisioningUpdates
