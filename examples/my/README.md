
### Make (cross-compile)

For every new project, and every time you make changes to your `my.pro` file,
run this script for creating/updating an Xcode project:

```
  ./make-xcodeproject.sh
```
The following 2 scripts will cross-compile the Lisp and Qt5 files and create an
app bundle, ready to be deployed to the device (no IPA file needed):

```
  ./ecl-ios.sh
  ./xcodebuild.sh
```

If the build succeeded, you can deploy the app directly to your device: it will
use **wifi** if not connected via **usb**:

```
  ./deploy-app.sh
```

N.B: Always run script `clean-lisp-files.sh` after changing your ECL version!


### Debugging

Since `ios-deploy` also offers command line debugging, you may use these
scripts to build a debug version, deploy and launch it from the debugger:

```
  ./debug-xcodebuild.sh
  ./debug-deploy.sh

  ./debug-app.sh
```
This is (of course) only needed if your ECL crashes your app; for debugging
the app itself, just run it on the desktop, see below.


### Developing your app: variant (1) - desktop

To just run this example on the desktop (requires ECL and EQL5 desktop versions
to be installed), do:

```
  $ eql5 run.lisp -qtpl
```

Simply use `:qq` for quitting the desktop app. (If the app breaks on startup, a
simple `:c` might be sufficient to continue; some features might not be present
on the desktop.)

For interactive development of both Lisp and QML, you should use Slime (see
docu in EQL5 desktop how to start Swank and connect to it).

So, to run this example in Slime, do:

```
  $ cd EQL5-iOS/examples/my
  $ eql5 ~/slime/eql5-start-swank run.lisp
```

Now, in Emacs connect to the above Swank, and hack both Lisp (as usual) and QML
using `(my:reload-qml)` after changes made to QML.


### Developing your app: variant (2) - device

![Emacs/Slime and Qt Creator](img/interactive.png)

* ensure you have **wifi** access, and disconnect usb

* start **myApp** on the phone and tap on the REPL switch

* in the **myApp** REPL, eval `:s` (to start Swank)

The first time will take longer (byte-compiling Swank). If you get a "bind"
error message from `sockets`, try to repeat the above command `:s` (just don't
give up easily).

* on the **desktop**, connect from Slime `M-x slime-connect`, using the **IP** displayed on the phone

* on the **desktop**, run `$ ./web-server.sh` (requires python 3)

Now use `lisp/my.lisp` and `qml/my.qml` for development.

* to load Lisp files located on the desktop computer, use `load*` (instead of
`load`)

example:
```
  EQL-USER> (load* "lisp/my") ; file ending defaults to ".lisp"
```
* to reload QML (after saving the changes) eval `:r` in Slime on the desktop

The first time you load something from the desktop, it will prompt for the last
number of your **desktop wifi ip** (as shown by e.g. `ifconfig`)

For editing QML, the best editor available is certainly **Qt Creator**.

In QML you may (of course) include/add other QML files (or images/fonts/etc.);
just use local path names for them.

If you want to force closing the app (during development), run this in the app
REPL:
```
  (qq) ; short for (eql:qquit), which in turn calls C exit(0)
```

**N.B:** Only use **wifi** for reloading QML as described above: if your device
is connected via **usb** during QML reloading, binary files (like images) will
not be transmitted by the local web server!


### Internationalization (i18n)

The basics of translations are aleady covered by the docu in EQL5 desktop
`~/eql5/doc/QtLinguist.htm`; if you are already familiar with it, the below
description should suffice:

* in Lisp, wrap every string to be translated in macro `eql:tr`
* in QML, wrap every string to be translated in `qsTr()`
```
  (print (tr "Lisp string"))

  text: qsTr("QML string")
```

Make sure you load `tr.lisp` before compiling (here: in `make.lisp`):
```
  (load "tr.lisp")
```
In order to collect all Lisp strings, you need a clean recompile of all Lisp
files (see `clean-lisp-files.sh`). This will create a dummy file for the
translations called `tr.h`.

Now you need to create/update your translation file(s). This is done by
simply running script
```
  ./my-lupdate`
```
The above generated file (e.g. `my_es.ts`) can now be opened in Qt
**linguist**.

After translating, the last step is to run:
```
  ./my-lrelease
```
This will create e.g. `my_es.qm`, ready to be included in the resource file
(see `my.qrc`).

In `build/main.mm` you can find an example how to automatically load the
translation file for the user defined locale of the mobile device.


### ASDF

The new integration (added 2020) should work for all ASDF systems supported
on mobile.

For every dependency of your ASDF system (see `my.asd`), you should add a line
in `dependencies.lisp` how to load it (usually via Quicklisp). It will then
be installed automatically if not present yet.

--

If you want to use libraries that depend on the ECL contribs, like `:sockets`,
you need to build a separate library and load it after program startup. For
an example, see `load-from-url` in `make.lisp`, `lisp/load-from-url.lisp`,
`lisp/my-ini.lisp`, `my.pro`, `build/c_bridge.cpp`. You can use
`make-libs.lisp` for building your separate library.

See `libs.asd` and `dependencies-libs.lisp` and use this variant to build the
libs (instead of the app):

```
  ./ecl-ios.sh libs
```

Unfortunately the above `libs` approach is a little complex and envolves
several steps, because we can't use `*.fas` files in iOS.


### Quicklisp note

If you want to have Quicklisp installed on your iOS device, you (currently)
need to do it like here: provide a pre-installed Quicklisp version, and just
load it (see `assets/`).

Unfortunately, downloading Quicklisp directly on the device crashes ECL
(happens only on iOS); see also note in `lisp/ini.lisp`, function `quicklisp`.
