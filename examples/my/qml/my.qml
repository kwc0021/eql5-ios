// all interesting QML modules (for playing around)

import QtQuick 2.10
import QtQuick.Controls 2.10
import QtQuick.Window 2.2
import QtQuick.Layouts 1.10
import QtMultimedia 5.10
import QtSensors 5.10
import QtPositioning 5.10
import QtWebView 1.10
import "ext/" as Ext
import EQL5 1.0

StackView {
    id: main
    objectName: "main"
    initialItem: mainRect
    focus: true

    property bool isPhone: (Math.max(width, height) < 1000) // trivial but seems reliable

    // show/hide dialogs (using StackView)
    // (we can't call 'push()'/'pop()' from Lisp: no C++ interface, so we call JS from Lisp instead)

    function pushFileDialog() { main.push(fileDialogInstance) }
    function popDialog()      { main.pop() }

    Ext.FileBrowser { id: fileDialogInstance }

    Keys.onPressed: {
        if(event.key === Qt.Key_Back) {
            event.accepted = true
            Lisp.call("eql:back-pressed")
        }
    }

    // fonts

    FontLoader { id: fontAwesome;  source: "fonts/fontawesome-webfont.ttf" } // icons
    FontLoader { id: fontHack;     source: "fonts/Hack-Regular.ttf" }        // code
    FontLoader { id: fontHackBold; source: "fonts/Hack-Bold.ttf" }

    Rectangle {
        id: mainRect
        color: "lightcyan"

        Ext.Repl {}

        // your QML

    }
}
