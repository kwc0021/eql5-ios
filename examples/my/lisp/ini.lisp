;;;
;;; Includes everything for Quicklisp and Swank.
;;;

(in-package :eql)

;; the following 2 functions are stolen (and modified) from :cl-fad

(defun copy-stream (from to)
  (let ((buf (make-array 8192 :element-type (stream-element-type from))))
    (loop
      (let ((pos (read-sequence buf from)))
        (when (zerop pos)
          (return))
        (write-sequence buf to :end pos))))
  (values))

(defun copy-file (from to)
  (let ((element-type '(unsigned-byte 8)))
    (with-open-file (in from :element-type element-type)
      (with-open-file (out to :element-type element-type
                           :direction :output :if-exists :supersede)
        (copy-stream in out)
        (finish-output out)
        (= (file-length in)
           (file-length out))))))

(defvar *bundle-root* (namestring *default-pathname-defaults*)) ; capture here, will change

(defun copy-asset-files (dir-name origin)
  "Copy files to writeable home path."
  (flet ((to-local (name)
           (merge-pathnames (x:cc "../" (subseq name (length origin))))))
    (dolist (dir (directory (merge-pathnames "*/" dir-name)))   ; directories
      (let ((name (namestring dir)))
        (ensure-directories-exist (to-local name))
        (copy-asset-files name origin)))
    (dolist (file (directory (merge-pathnames "*.*" dir-name))) ; files
      (let* ((from (namestring file))
             (to (to-local from)))
        (unless (ignore-errors (copy-file from to))
          (qmsg (format nil "Error copying asset file: ~S" from))
          (return-from copy-asset-files)))))
  t)

(defun touch-file (name)
  (open name :direction :probe :if-does-not-exist :create))

(defun post-install ()
  (flet ((%merge (dir)
           (namestring (merge-pathnames dir *bundle-root*))))
    (let ((dir (%merge "assets/")))
      (when (copy-asset-files dir dir)
        (touch-file ".eql5-ini")
        :done))))

(defun ini ()
  #+ios
  (setf *default-pathname-defaults* (user-homedir-pathname)) ; set to writable path
  (si:install-bytecodes-compiler)
  (let ((.eclrc ".eclrc"))
    (if (probe-file .eclrc)
        (load .eclrc)
        (touch-file .eclrc)))
  (unless (probe-file ".eql5-ini")
    (post-install)))

;; Quicklisp setup (stolen from 'ecl-android')

(defun sym (symbol package)
  (intern (symbol-name symbol) package))

#+ios
(defvar *c-bridge* (qfind-child (qapp) "c_bridge"))

(defun load-lib (name)
  (! "load" (:qt *c-bridge*) name))

(export 'load-lib)

(defun asdf ()
  (unless (find-package :asdf)
    (setf (logical-pathname-translations "SYS")
          (list (list "sys:**;*.*"
                      (merge-pathnames "**/*.*"
                                       (user-homedir-pathname)))))
    (load-lib "asdf")
    (in-package :eql-user))
  :asdf)

(export 'asdf)

(defun quicklisp ()
  (asdf)
  (unless (find-package :quicklisp)
    (load "quicklisp/setup"))
    #|
    ;; previous version; unfortunately, this crashes with current ECL after
    ;; downloading "asdf.lisp" from QL; to work around this, we need to 
    ;; provide all Quicklisp files ourselves (see assets) and just load it
    ;; (see above)
    (load-lib "ecl-quicklisp")
    |#
    ;; replace interpreted function with precompiled one from DEFLATE
    (setf (symbol-function (sym 'gunzip :ql-gunzipper))
          (symbol-function (sym 'gunzip :deflate)))
  :quicklisp)

(export 'quicklisp)

;; Swank setup (stolen from 'ecl-android')

(defun swank/create-server (interface port dont-close style)
  (funcall (sym 'create-server :swank)
           :interface interface
           :port port
           :dont-close dont-close
           :style style))

(defun start-swank (&key (interface (ip-string)) log-events (load-contribs t) (setup t)
                         (delete t) (quiet t) (port 4005) (dont-close t) (style :spawn))
  "Pass :interface as an IP string, if you want to use a specific IP address."
  (unless (find-package :swank)
    (asdf)
    (load "quicklisp/local-projects/slime/swank-loader"))
  (funcall (sym 'init :swank-loader)
           :load-contribs load-contribs
           :setup setup
           :delete delete
           :quiet quiet)
  (setf (symbol-value (sym '*log-events* :swank)) log-events)
  (eval (read-from-string "(swank/backend:defimplementation swank/backend:lisp-implementation-program () \"org.lisp.ecl\")"))
  (setf *package* (find-package :eql-user))
  (if (eql :spawn style)
      (swank/create-server interface port dont-close style)
      (mp:process-run-function
       "SLIME-listener"
       (lambda () (swank/create-server interface port dont-close style))))
  (qsingle-shot 1000 (lambda ()
                       (eval:append-output (format nil ";; connect to: ~A~%" interface))))
  (disable-idle-timer)
  (values))

(defun ip-string ()
  "Tries to find the local WiFi address. If the result is not unique, an empty string is returned (no guesses)."
  (let (ip4)
    (dolist (ad (|allAddresses.QNetworkInterface|))
      (when (= (|protocol| ad) |QAbstractSocket.IPv4Protocol|)
        (let ((str (|toString| ad)))
          (when (x:starts-with "192.168.1." str)
            (return-from ip-string str))
          (unless (string= str "127.0.0.1")
            (push str ip4)))))
    (if (rest ip4)
        ""
        (first ip4))))

(defun disable-idle-timer (&optional (disable t))
  (! "disableIdleTimer" (:qt *c-bridge*) disable))

(defun stop-swank ()
  (when (find-package :swank)
    (funcall (sym 'stop-server :swank) 4005)
    (disable-idle-timer nil)
    :stopped))

(export 'start-swank)
(export 'disable-idle-timer)
(export 'stop-swank)

;; convenience

(define-symbol-macro :a (asdf))
(define-symbol-macro :s (start-swank))
(define-symbol-macro :q (quicklisp))
(define-symbol-macro :l (funcall (sym 'load-file :dialogs)))
(define-symbol-macro :f (funcall (sym 'get-file-name :dialogs)))
(define-symbol-macro :r (my:reload-qml))

(defun help ()
  (format t "~%~
             ~%  :a  (eql:asdf)~
             ~%  :s  (eql:start-swank)~
             ~%  :q  (eql:quicklisp)         ; will install/load it~
             ~%  :l  (dialogs:load-file)     ; load~
             ~%  :f  (dialogs:get-file-name) ; see dialogs:*file-name*~
             ~%  :r  (my:reload-qml)         ; see docu")
  (values))

(export 'help)

;;; adapt all relevant paths to iOS specific values

(ext:package-lock :common-lisp nil)

(defvar *user-homedir-pathname-orig* (symbol-function 'user-homedir-pathname))

(defun user-homedir-pathname (&optional host)
  #+ios
  (merge-pathnames "Library/" (funcall *user-homedir-pathname-orig* host)
  #-ios
  *default-pathname-defaults*))

(ext:package-lock :common-lisp t)

#+ios
(dolist (el '(("XDG_DATA_HOME"   . "")
              ("XDG_CONFIG_HOME" . "")
              ("XDG_DATA_DIRS"   . "")
              ("XDG_CONFIG_DIRS" . "")
              ("XDG_CACHE_HOME"  . ".cache")))
  (ext:setenv (car el) (namestring (merge-pathnames (cdr el) (user-homedir-pathname)))))

;; quit app

(defun back-pressed () ; called from QML
  (or (funcall (sym 'pop-dialog :dialogs))
      (qquit)))

(export 'back-pressed)

;; ini

(qlater 'ini)
