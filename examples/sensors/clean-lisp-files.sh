# run this script every time you change your ECL version

rm *.o                                                        # package.o
find lisp/ -name "*.o" -delete                                # all lisp files
find ~/quicklisp/dists/quicklisp/software -name "*.o" -delete # all cross-compiled files
