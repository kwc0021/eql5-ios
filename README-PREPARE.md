
*You probably want the new, cross-platform version of this, see*
[LQML](https://gitlab.com/eql/lqml).



Acknowledgements
----------------

This project requires *exactly* the following development environment:
(in order to match the pre-built and included EQL5 libraries for iOS)

* ECL 21.2.1 [official release](https://common-lisp.net/project/ecl/static/files/release/)
* Qt 5.13.2 [mac-x64.dmg](https://download.qt.io/archive/qt/5.13/5.13.2/)
* Xcode 11

Note: if you don't want to use the prebuilt libs in `lib/`, or you want to use
a Qt version different from Qt 5.13, please see:

[EQL5-iOS-libs](https://gitlab.com/eql/EQL5/-/tree/iOS-libs)

for building them on your own.

**Xcode 12** note: for Qt 5.13 (which is used here) you need to set your build
system to the legacy one:
`File -> Project Settings... -> Build System: Legacy Build System (Deprecated)`;
if `make-xcodeproject.sh` gives errors using the Xcode 12 command line tools,
you need to first delete `.qmake.stash`, then use Xcode 11 command line tools
for that script; after creating the Xcode project, you can build with either
Xcode 11 or Xcode 12.

For building in Xcode (deployment), you need to set **Enable Bitcode** to
**No** in `Build Settings` / `Build Options`.



Step 1. Build cross-compiled ECL for iOS
----------------------------------------

* extract ECL in `EQL5-iOS/ecl`
* copy the 3 scripts from `EQL5-iOS/scripts/` to `EQL5-iOS/ecl/`
* switch to `ecl/` and  run `./1-make-ecl-host.sh`

Edit `src/c/unixsys.d` and search for `HAVE_SYSTEM`; right before
the function definition inside which it occurs, put this line:
```
  #undef HAVE_SYSTEM
```
Edit `src/c/thread/process.d`, search for `pthread_attr_init` (line 590) and
add the following below that line:
```
  pthread_attr_setstacksize(&pthreadattr, 1933312); // double default size: 2 * 236 * 4096
```

* run `./2-make-ecl-ios.sh`
* run `./3-copy-to-assets.sh` (needs ECL_IOS to be set first, see below)


Step 2. Settings in your `~/.profile`
-------------------------------------

Example:

```
  export ECL_IOS='/Users/<username>/EQL5-iOS/ecl/ecl-ios'
  export QT_VERSION=5.13.2

  alias qmake-ios='/Users/<username>/Qt"$QT_VERSION"/"$QT_VERSION"/ios/bin/qmake'
```

Optionally you may set (to display QML files directly during development):

```
  alias qml='/Users/<username>/Qt"$QT_VERSION"/"$QT_VERSION"/clang_64/bin/qml.app/Contents/MacOS/qml'

```

(Use `source ~/.profile` to make new environment variables take effect in your
current terminal session.)



Step 3. Install "ios-deploy"
----------------------------

For both deploying and eventual low-level debugging (if the app crashes on the
device), you will absolutely need this great tool:

[github.com/ios-control/ios-deploy](https://github.com/ios-control/ios-deploy)

--

Now you should be able to build the examples.

