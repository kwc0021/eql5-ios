/*      Compiler: ECL 21.2.1                                          */
/*      Source: EXT:SOCKETS;SOCKETS.LISP                              */
#include <ecl/ecl-cmp.h>
#include "ext/sockets.eclh"
/*      function definition for FF-SOCKET                             */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L1ff_socket(cl_object v1, cl_object v2, cl_object v3)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   int v4;
   ecl_disable_interrupts();v4=socket(ecl_to_int(v1),ecl_to_int(v2),ecl_to_int(v3));ecl_enable_interrupts();
   value0 = ecl_make_int(v4);
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      function definition for FF-LISTEN                             */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L2ff_listen(cl_object v1, cl_object v2)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   int v3;
   ecl_disable_interrupts();v3=listen(ecl_to_int(v1),ecl_to_int(v2));ecl_enable_interrupts();
   value0 = ecl_make_int(v3);
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      function definition for FF-CLOSE                              */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L3ff_close(cl_object v1)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   int v2;
   ecl_disable_interrupts();v2=close(ecl_to_int(v1));ecl_enable_interrupts();
   value0 = ecl_make_int(v2);
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      function definition for SPLIT                                 */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L5split(cl_narg narg, cl_object v1string, ...)
{
 cl_object T0, T1;
 cl_object env0 = ECL_NIL;
 cl_object CLV0;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 if (ecl_unlikely(narg<1)) FEwrong_num_arguments_anonym();
 if (ecl_unlikely(narg>3)) FEwrong_num_arguments_anonym();
 {
  cl_object v2max;
  va_list args; va_start(args,v1string);
  {
   int i = 1;
   if (i >= narg) {
    v2max = ECL_NIL;
   } else {
    i++;
    v2max = va_arg(args,cl_object);
   }
   if (i >= narg) {
    env0 = ECL_NIL;
    CLV0 = env0 = CONS(VV[7],env0);               /*  WS              */
   } else {
    i++;
    env0 = ECL_NIL;
    CLV0 = env0 = CONS(va_arg(args,cl_object),env0); /*  WS           */
   }
  }
  va_end(args);
  {
   cl_object v3;
   v3 = ecl_make_cclosure_va((cl_objectfn)LC4is_ws,env0,Cblock,1);
   {
    cl_object v4start;
    v4start = ECL_NIL;
    {
     cl_object v5index;
     v5index = ECL_NIL;
     {
      cl_fixnum v6word_count;
      v6word_count = 0;
      {
       cl_object v7;
       cl_object v8;
       v7 = ecl_list1(ECL_NIL);
       v8 = v7;
       v4start = cl_position_if_not(2, v3, v1string);
L7:;
       if (Null(v4start)) { goto L14; }
       if (Null(v2max)) { goto L16; }
       T0 = ecl_make_integer((v6word_count)+1);
       if (!(ecl_number_equalp(T0,v2max))) { goto L16; }
       v5index = ECL_NIL;
       goto L12;
L16:;
       v5index = cl_position_if(4, v3, v1string, ECL_SYM("START",1337), v4start);
       goto L12;
L14:;
       v5index = ECL_NIL;
       goto L12;
L12:;
       if ((v4start)!=ECL_NIL) { goto L19; }
       goto L8;
L19:;
       {
        cl_object v9;
        v9 = v8;
        if (ecl_unlikely(ECL_ATOM(v9))) FEtype_error_cons(v9);
        value0 = ECL_NIL;
        cl_env_copy->nvalues = 0;
        T0 = v9;
       }
       T1 = cl_subseq(3, v1string, v4start, v5index);
       v8 = ecl_list1(T1);
       (ECL_CONS_CDR(T0)=v8,T0);
       {
        cl_object v9;
        v9 = ecl_make_integer((v6word_count)+1);
        {
         bool v10;
         v10 = ECL_FIXNUMP(v9);
         if (ecl_unlikely(!(v10)))
         FEwrong_type_argument(ECL_SYM("FIXNUM",374),v9);
         value0 = ECL_NIL;
         cl_env_copy->nvalues = 0;
        }
        v6word_count = ecl_fixnum(v9);
       }
       if ((v5index)!=ECL_NIL) { goto L30; }
       goto L8;
L30:;
       v4start = cl_position_if_not(4, v3, v1string, ECL_SYM("START",1337), v5index);
       goto L7;
L8:;
       value0 = ecl_cdr(v7);
       cl_env_copy->nvalues = 1;
       return value0;
      }
     }
    }
   }
  }
 }
}
/*      closure IS-WS                                                 */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC4is_ws(cl_narg narg, cl_object v1char, ...)
{
 cl_object CLV0;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object env0 = cl_env_copy->function->cclosure.env;
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 /* Scanning closure data ... */
 CLV0 = env0;                                     /*  WS              */
 { /* ... closure scanning finished */
 if (ecl_unlikely(narg!=1)) FEwrong_num_arguments_anonym();
 {
TTL:
  {
   cl_object v2;
   v2 = ecl_function_dispatch(cl_env_copy,VV[168])(2, ECL_CONS_CAR(CLV0), ecl_make_fixnum(0)) /*  MAKE-SEQ-ITERATOR */;
L2:;
   if ((v2)!=ECL_NIL) { goto L4; }
   value0 = ECL_NIL;
   cl_env_copy->nvalues = 1;
   return value0;
L4:;
   {
    cl_object v3;
    v3 = ecl_function_dispatch(cl_env_copy,VV[169])(2, ECL_CONS_CAR(CLV0), v2) /*  SEQ-ITERATOR-REF */;
    if (!(ecl_eql(v1char,v3))) { goto L6; }
    value0 = v3;
    cl_env_copy->nvalues = 1;
    return value0;
   }
L6:;
   v2 = ecl_function_dispatch(cl_env_copy,VV[170])(2, ECL_CONS_CAR(CLV0), v2) /*  SEQ-ITERATOR-NEXT */;
   goto L2;
  }
 }
 }
}
/*      local function HOST-ENT-ADDRESS                               */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC6host_ent_address(cl_object v1host_ent)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[172])(1, v1host_ent) /*  HOST-ENT-ADDRESSES */;
  value0 = ecl_car(T0);
  cl_env_copy->nvalues = 1;
  return value0;
 }
}
/*      function definition for GET-HOST-BY-NAME                      */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L7get_host_by_name(cl_object v1host_name)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v3;                                  /*  ERRNO           */
   cl_object v4;                                  /*  CANONICAL-NAME  */
   cl_object v5;                                  /*  ADDRESSES       */
   cl_object v6;                                  /*  ALIASES         */
   {
    cl_object v7;
    v7 = si_copy_to_simple_base_string(v1host_name);
    {
     int v8;
     cl_object v9;
     cl_object v10;
     cl_object v11;
     
{
    struct addrinfo hints;
    struct addrinfo *result;
    cl_object host_name = ECL_NIL;
    cl_object aliases = ECL_NIL;
    cl_object addresses = ECL_NIL;
    int err;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;                     /* IPv4 */
    hints.ai_socktype = 0;                         /* Any type */
    hints.ai_protocol = 0;                         /* Any protocol */
    hints.ai_flags = (AI_CANONNAME);               /* Get cannonname */
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    ecl_disable_interrupts();
    err = getaddrinfo(ecl_base_string_pointer_safe(v7), NULL, &hints, &result);
    ecl_enable_interrupts();

    if (err == 0) {
        struct addrinfo *rp;

        for (rp = result; rp != NULL; rp = rp->ai_next) {
            if ( (rp == result) && (rp->ai_canonname != 0) ) {  /* first one may hold cannonname */
                host_name = ecl_make_simple_base_string( rp->ai_canonname, -1 );
            }
            struct sockaddr_in *ipv4 = (struct sockaddr_in *)rp->ai_addr;
            uint32_t ip = ntohl( ipv4->sin_addr.s_addr );
            cl_object vector = cl_make_array(1,ecl_make_fixnum(4));
            ecl_aset(vector,0, ecl_make_fixnum( ip>>24 ));
            ecl_aset(vector,1, ecl_make_fixnum( (ip>>16) & 0xFF));
            ecl_aset(vector,2, ecl_make_fixnum( (ip>>8) & 0xFF));
            ecl_aset(vector,3, ecl_make_fixnum( ip & 0xFF ));
            addresses = cl_adjoin(4, vector, addresses, ECL_SYM("TEST",1343), ECL_SYM("EQUALP",338));
            if ( rp->ai_canonname != 0 ) {
                cl_object alias = ecl_make_simple_base_string( rp->ai_canonname, -1 );
                aliases = CONS(alias, aliases);
            }
        }
        freeaddrinfo(result);
    }
    v8= err;
    v9= host_name;
    v10= addresses;
    v11= aliases;
}
     cl_env_copy->values[0] = ecl_make_int(v8);
     cl_env_copy->values[1] = v9;
     cl_env_copy->values[2] = v10;
     cl_env_copy->values[3] = v11;cl_env_copy->nvalues = 4;
     value0 = cl_env_copy->values[0];
    }
   }
   {
    v3 = value0;
    v4 = cl_env_copy->values[1];
    v5 = cl_env_copy->values[2];
    v6 = cl_env_copy->values[3];
   }
   if (!((ecl_fixnum(v3))==(0))) { goto L3; }
   value0 = v4;
   if ((value0)!=ECL_NIL) { goto L7; }
   T0 = v1host_name;
   goto L5;
L7:;
   T0 = value0;
   goto L5;
L5:;
   value0 = (cl_env_copy->function=(ECL_SYM("MAKE-INSTANCE",951)->symbol.gfdef))->cfun.entry(9, VV[9], ECL_SYM("NAME",1300), T0, VV[12], v6, ECL_SYM("TYPE",1346), ecl_symbol_value(VV[0]), VV[13], v5) /*  MAKE-INSTANCE */;
   return value0;
L3:;
   value0 = L46name_service_error(2, VV[14], v3);
   return value0;
  }
 }
}
/*      function definition for GET-HOST-BY-ADDRESS                   */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L8get_host_by_address(cl_object v1address)
{
 cl_object T0, T1, T2, T3, T4;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  goto L3;
L2:;
  si_assert_failure(1, VV[16]);
L3:;
  if (!(ECL_VECTORP(v1address))) { goto L9; }
  {
   cl_fixnum v2;
   v2 = ecl_length(v1address);
   if ((v2)==(4)) { goto L6; }
   goto L7;
  }
L9:;
  goto L7;
L7:;
  goto L2;
L6:;
  {
   cl_object v3;                                  /*  ERRNO           */
   cl_object v4;                                  /*  NAME            */
   {
    cl_object v5;
    v5 = v1address;
    T1 = v5;
    {
     cl_fixnum v6;
     if (ecl_unlikely((0)>=(v5)->vector.dim))
           FEwrong_index(ECL_NIL,v5,-1,ecl_make_fixnum(0),(v5)->vector.dim);
     value0 = ECL_NIL;
     cl_env_copy->nvalues = 0;
     v6 = 0;
     T0 = ecl_aref_unsafe(T1,v6);
    }
   }
   {
    cl_object v5;
    v5 = v1address;
    T2 = v5;
    {
     cl_fixnum v6;
     if (ecl_unlikely((1)>=(v5)->vector.dim))
           FEwrong_index(ECL_NIL,v5,-1,ecl_make_fixnum(1),(v5)->vector.dim);
     value0 = ECL_NIL;
     cl_env_copy->nvalues = 0;
     v6 = 1;
     T1 = ecl_aref_unsafe(T2,v6);
    }
   }
   {
    cl_object v5;
    v5 = v1address;
    T3 = v5;
    {
     cl_fixnum v6;
     if (ecl_unlikely((2)>=(v5)->vector.dim))
           FEwrong_index(ECL_NIL,v5,-1,ecl_make_fixnum(2),(v5)->vector.dim);
     value0 = ECL_NIL;
     cl_env_copy->nvalues = 0;
     v6 = 2;
     T2 = ecl_aref_unsafe(T3,v6);
    }
   }
   {
    cl_object v5;
    v5 = v1address;
    T4 = v5;
    {
     cl_fixnum v6;
     if (ecl_unlikely((3)>=(v5)->vector.dim))
           FEwrong_index(ECL_NIL,v5,-1,ecl_make_fixnum(3),(v5)->vector.dim);
     value0 = ECL_NIL;
     cl_env_copy->nvalues = 0;
     v6 = 3;
     T3 = ecl_aref_unsafe(T4,v6);
    }
   }
   {
    int v5;
    cl_object v6;
    
{
    struct sockaddr_in addr;
    socklen_t addr_len = (socklen_t)sizeof(struct sockaddr_in);
    char host[NI_MAXHOST];
    int err;

    fill_inet_sockaddr(&addr, 0, ecl_to_int(T0), ecl_to_int(T1), ecl_to_int(T2), ecl_to_int(T3));

    ecl_disable_interrupts();
    err = getnameinfo((struct sockaddr *) &addr, addr_len, host, NI_MAXHOST, NULL, 0, NI_NAMEREQD);
    ecl_enable_interrupts();

    v5= err;
    v6= err ? ECL_NIL : ecl_make_simple_base_string(host,-1);
}
    cl_env_copy->values[0] = ecl_make_int(v5);
    cl_env_copy->values[1] = v6;cl_env_copy->nvalues = 2;
    value0 = cl_env_copy->values[0];
   }
   {
    v3 = value0;
    v4 = cl_env_copy->values[1];
   }
   if (!((ecl_fixnum(v3))==(0))) { goto L32; }
   T0 = ecl_list1(v1address);
   value0 = (cl_env_copy->function=(ECL_SYM("MAKE-INSTANCE",951)->symbol.gfdef))->cfun.entry(9, VV[9], ECL_SYM("NAME",1300), v4, VV[12], ECL_NIL, ECL_SYM("TYPE",1346), ecl_symbol_value(VV[0]), VV[13], T0) /*  MAKE-INSTANCE */;
   return value0;
L32:;
   value0 = L46name_service_error(2, VV[17], v3);
   return value0;
  }
 }
}
/*      local function LAMBDA31                                       */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC9__lambda31()
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  value0 = cl_error(1, VV[20]);
  return value0;
 }
}
/*      local function PRINT-OBJECT                                   */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC11print_object(cl_object v1object, cl_object v2stream)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 cl_object CLV0, CLV1;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
  env0 = ECL_NIL;
  CLV0 = env0 = CONS(v1object,env0);              /*  OBJECT          */
  CLV1 = env0 = CONS(v2stream,env0);              /*  STREAM          */
  {
   cl_object v3;
   v3 = ecl_make_cclosure_va((cl_objectfn)LC10si___print_unreadable_object_body_,env0,Cblock,0);
   T0 = ECL_CONS_CAR(CLV1);
   value0 = si_print_unreadable_object_function(ECL_CONS_CAR(CLV0), T0, ECL_T, ECL_T, v3);
   return value0;
  }
 }
}
/*      closure .PRINT-UNREADABLE-OBJECT-BODY.                        */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC10si___print_unreadable_object_body_(cl_narg narg, ...)
{
 cl_object T0;
 cl_object CLV0, CLV1;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object env0 = cl_env_copy->function->cclosure.env;
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 /* Scanning closure data ... */
 CLV1 = env0;                                     /*  STREAM          */
 CLV0 = _ecl_cdr(CLV1);
 { /* ... closure scanning finished */
 if (ecl_unlikely(narg!=0)) FEwrong_num_arguments_anonym();
 {
TTL:
  if (Null(cl_slot_boundp(ECL_CONS_CAR(CLV0), VV[21]))) { goto L1; }
  ecl_princ(VV[22],ECL_CONS_CAR(CLV1));
  T0 = cl_slot_value(ECL_CONS_CAR(CLV0), VV[21]);
  value0 = ecl_princ(T0,ECL_CONS_CAR(CLV1));
  cl_env_copy->nvalues = 1;
  return value0;
L1:;
  value0 = ecl_princ(VV[23],ECL_NIL);
  cl_env_copy->nvalues = 1;
  return value0;
 }
 }
}
/*      local function SHARED-INITIALIZE                              */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC12shared_initialize(cl_narg narg, cl_object v1socket, cl_object v2slot_names, ...)
{
 cl_object T0, T1;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 if (ecl_unlikely(narg<2)) FEwrong_num_arguments_anonym();
 {
  cl_object v3protocol;
  cl_object v4type;
  ecl_va_list args; ecl_va_start(args,v2slot_names,narg,2);
  {
   cl_object keyvars[4];
   cl_parse_key(args,2,LC12shared_initializekeys,keyvars,NULL,TRUE);
   ecl_va_end(args);
   v3protocol = keyvars[0];
   v4type = keyvars[1];
  }
  {
   cl_object v5proto_num;
   cl_object v6fd;
   if (Null(v3protocol)) { goto L2; }
   if (Null(cl_keywordp(v3protocol))) { goto L2; }
   T0 = ecl_symbol_name(v3protocol);
   T1 = cl_string_downcase(1, T0);
   v5proto_num = L19get_protocol_by_name(T1);
   goto L1;
L2:;
   if (Null(v3protocol)) { goto L5; }
   v5proto_num = v3protocol;
   goto L1;
L5:;
   v5proto_num = ecl_make_fixnum(0);
L1:;
   if (Null(cl_slot_boundp(v1socket, VV[21]))) { goto L12; }
   value0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   goto L10;
L12:;
   value0 = ECL_NIL;
   goto L10;
L10:;
   if ((value0)!=ECL_NIL) { goto L9; }
   T0 = ecl_function_dispatch(cl_env_copy,VV[178])(1, v1socket) /*  SOCKET-FAMILY */;
   {
    cl_object v7;
    value0 = v4type;
    if ((value0)!=ECL_NIL) { goto L17; }
    v7 = ecl_function_dispatch(cl_env_copy,VV[179])(1, v1socket) /*  SOCKET-TYPE */;
    goto L15;
L17:;
    v7 = value0;
    goto L15;
L15:;
    if (!(ecl_eql(v7,VV[24]))) { goto L19; }
    T1 = ecl_make_int(SOCK_DGRAM);
    goto L14;
L19:;
    if (!(ecl_eql(v7,ECL_SYM("STREAM",1340)))) { goto L22; }
    T1 = ecl_make_int(SOCK_STREAM);
    goto L14;
L22:;
    T1 = si_ecase_error(v7, VV[27]);
   }
L14:;
   v6fd = L1ff_socket(T0, T1, v5proto_num);
   goto L7;
L9:;
   v6fd = value0;
   goto L7;
L7:;
   if (!(ecl_number_equalp(v6fd,ecl_make_fixnum(-1)))) { goto L25; }
   L45socket_error(VV[28]);
L25:;
   (cl_env_copy->function=ECL_CONS_CAR(VV[180]))->cfun.entry(3, v6fd, v1socket, VV[21]) /*  (SETF SLOT-VALUE) */;
   value0 = (cl_env_copy->function=ECL_CONS_CAR(VV[180]))->cfun.entry(3, v5proto_num, v1socket, VV[29]) /*  (SETF SLOT-VALUE) */;
   return value0;
  }
 }
}
/*      local function SOCKET-LISTEN                                  */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC13socket_listen(cl_object v1socket, cl_object v2backlog)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v3r;
   T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   v3r = L2ff_listen(T0, v2backlog);
   if (!(ecl_number_equalp(v3r,ecl_make_fixnum(-1)))) { goto L2; }
   value0 = L45socket_error(VV[42]);
   return value0;
L2:;
   value0 = ECL_NIL;
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      local function SOCKET-CLOSE-LOW-LEVEL                         */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC14socket_close_low_level(cl_object v1socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
  value0 = L3ff_close(T0);
  return value0;
 }
}
/*      local function SOCKET-CLOSE                                   */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC15socket_close(cl_narg narg, cl_object v1socket, ...)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 if (ecl_unlikely(narg<1)) FEwrong_num_arguments_anonym();
 {
  cl_object v2abort;
  ecl_va_list args; ecl_va_start(args,v1socket,narg,1);
  {
   cl_object keyvars[2];
   cl_parse_key(args,1,LC15socket_closekeys,keyvars,NULL,TRUE);
   ecl_va_end(args);
   v2abort = keyvars[0];
  }
  {
   cl_object v3fd;
   v3fd = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   if ((v3fd)==(ecl_make_fixnum(-1))) { goto L2; }
   if (Null(cl_slot_boundp(v1socket, ECL_SYM("STREAM",801)))) { goto L5; }
   {
    cl_object v4stream;
    v4stream = cl_slot_value(v1socket, ECL_SYM("STREAM",801));
    T0 = cl_two_way_stream_input_stream(v4stream);
    cl_close(3, T0, ECL_SYM("ABORT",1217), v2abort);
    T0 = cl_two_way_stream_output_stream(v4stream);
    cl_close(3, T0, ECL_SYM("ABORT",1217), v2abort);
   }
   cl_slot_makunbound(v1socket, ECL_SYM("STREAM",801));
   goto L4;
L5:;
   T0 = ecl_function_dispatch(cl_env_copy,VV[41])(1, v1socket) /*  SOCKET-CLOSE-LOW-LEVEL */;
   if (!(ecl_number_equalp(T0,ecl_make_fixnum(-1)))) { goto L4; }
   L45socket_error(VV[43]);
L4:;
   value0 = (cl_env_copy->function=ECL_CONS_CAR(VV[180]))->cfun.entry(3, ecl_make_fixnum(-1), v1socket, VV[21]) /*  (SETF SLOT-VALUE) */;
   return value0;
L2:;
   value0 = ECL_NIL;
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      local function SOCKET-RECEIVE                                 */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC16socket_receive(cl_narg narg, cl_object v1socket, cl_object v2buffer, cl_object v3length, ...)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 if (ecl_unlikely(narg<3)) FEwrong_num_arguments_anonym();
 {
  cl_object v4oob;
  cl_object v5peek;
  cl_object v6waitall;
  cl_object v7element_type;
  ecl_va_list args; ecl_va_start(args,v3length,narg,3);
  {
   cl_object keyvars[8];
   cl_parse_key(args,4,LC16socket_receivekeys,keyvars,NULL,TRUE);
   ecl_va_end(args);
   v4oob = keyvars[0];
   v5peek = keyvars[1];
   v6waitall = keyvars[2];
   if (Null(keyvars[7])) {
    v7element_type = ECL_SYM("BYTE8",1358);
   } else {
    v7element_type = keyvars[3];
   }
  }
  if ((v2buffer)!=ECL_NIL) { goto L2; }
  if ((v3length)!=ECL_NIL) { goto L2; }
  cl_error(1, VV[44]);
L2:;
  {
   cl_object v8;
   cl_object v9;
   cl_object v10;
   cl_object v11;
   cl_object v12buffer;
   cl_object v13length;
   cl_object v14fd;
   cl_object v15trunc;
   value0 = v2buffer;
   if ((value0)!=ECL_NIL) { goto L7; }
   v8 = si_make_pure_array(v7element_type, v3length, ECL_NIL, ECL_NIL, ECL_NIL, ecl_make_fixnum(0));
   goto L5;
L7:;
   v8 = value0;
   goto L5;
L5:;
   value0 = v3length;
   if ((value0)!=ECL_NIL) { goto L11; }
   v9 = ecl_make_fixnum(ecl_length(v2buffer));
   goto L9;
L11:;
   v9 = value0;
   goto L9;
L9:;
   v10 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   T0 = ecl_function_dispatch(cl_env_copy,VV[179])(1, v1socket) /*  SOCKET-TYPE */;
   if (!(ecl_eql(T0,VV[24]))) { goto L15; }
   v11 = ECL_T;
   goto L14;
L15:;
   v11 = ECL_NIL;
L14:;
   v12buffer = v8;
   v13length = v9;
   v14fd = v10;
   v15trunc = v11;
   {
    cl_object v17;                                /*  LEN-RECV        */
    cl_object v18;                                /*  ERRNO           */
    cl_object v19;                                /*  VECTOR          */
    cl_object v20;                                /*  PORT            */
    {
     long v21;
     int v22;
     cl_object v23;
     int v24;
     
{
        int flags = ( (v4oob)!=ECL_NIL ? MSG_OOB : 0 )  |
                    ( (v5peek)!=ECL_NIL ? MSG_PEEK : 0 ) |
                    ( (v6waitall)!=ECL_NIL ? MSG_WAITALL : 0 ) |
                    ( (v15trunc)!=ECL_NIL ? MSG_TRUNC : 0 );
        cl_type type = ecl_t_of(v12buffer);
        ssize_t len;
        struct sockaddr_in sender;
        socklen_t addr_len = (socklen_t)sizeof(struct sockaddr_in);

        ecl_disable_interrupts();
        len = recvfrom(ecl_to_int(v14fd), wincoerce(char*, safe_buffer_pointer(v12buffer, ecl_to_int(v13length))),
                       ecl_to_int(v13length), flags, (struct sockaddr *)&sender, &addr_len);
        ecl_enable_interrupts();
        if (len >= 0) {
               if (type == t_vector) { v12buffer->vector.fillp = len; }
               else if (type == t_base_string) { v12buffer->base_string.fillp = len; }
        }
        v21= len;
        v22= errno;
        v23= ECL_NIL;
        v24= 0;

        if (len >= 0) {
                uint32_t ip = ntohl(sender.sin_addr.s_addr);
                uint16_t port = ntohs(sender.sin_port);
                cl_object vector = cl_make_array(1,ecl_make_fixnum(4));

                ecl_aset(vector,0, ecl_make_fixnum( ip>>24 ));
                ecl_aset(vector,1, ecl_make_fixnum( (ip>>16) & 0xFF));
                ecl_aset(vector,2, ecl_make_fixnum( (ip>>8) & 0xFF));
                ecl_aset(vector,3, ecl_make_fixnum( ip & 0xFF ));

                v23= vector;
                v24= port;
        }
}

     cl_env_copy->values[0] = ecl_make_long(v21);
     cl_env_copy->values[1] = ecl_make_int(v22);
     cl_env_copy->values[2] = v23;
     cl_env_copy->values[3] = ecl_make_int(v24);cl_env_copy->nvalues = 4;
     value0 = cl_env_copy->values[0];
    }
    {
     v17 = value0;
     v18 = cl_env_copy->values[1];
     v19 = cl_env_copy->values[2];
     v20 = cl_env_copy->values[3];
    }
    if (!(ecl_number_equalp(v17,ecl_make_fixnum(-1)))) { goto L22; }
    T0 = cl_list(2, ecl_symbol_value(VV[2]), ecl_symbol_value(VV[3]));
    if (Null(ecl_memql(v18,T0))) { goto L22; }
    value0 = ECL_NIL;
    cl_env_copy->nvalues = 1;
    return value0;
L22:;
    if (!(ecl_number_equalp(v17,ecl_make_fixnum(-1)))) { goto L25; }
    value0 = L45socket_error(VV[45]);
    return value0;
L25:;
    cl_env_copy->nvalues = 4;
    cl_env_copy->values[3] = v20;
    cl_env_copy->values[2] = v19;
    cl_env_copy->values[1] = v17;
    cl_env_copy->values[0] = v12buffer;
    return cl_env_copy->values[0];
   }
  }
 }
}
/*      local function LAMBDA69                                       */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC18__lambda69()
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC17__lambda70,ECL_NIL,Cblock,2);
   value0 = v1;
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      local function LAMBDA70                                       */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC17__lambda70(cl_object v1condition, cl_object v2stream)
{
 cl_object T0, T1;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[186])(1, v1condition) /*  UNKNOWN-PROTOCOL-NAME */;
  T1 = cl_prin1_to_string(T0);
  value0 = cl_format(3, v2stream, VV[47], T1);
  return value0;
 }
}
/*      function definition for GET-PROTOCOL-BY-NAME                  */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L19get_protocol_by_name(cl_object v1string_or_symbol)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v2string;
   cl_fixnum v3proto_num;
   v2string = cl_string(v1string_or_symbol);
   {
    cl_object v4;
    v4 = si_copy_to_simple_base_string(v2string);
    {
     int v5;
     {
                                 struct protoent *pe;
                                 pe = getprotobyname(ecl_base_string_pointer_safe(v4));
                                 v5= pe ? pe->p_proto : -1;
                               }
               
     v3proto_num = (cl_fixnum)(v5);
    }
   }
   if (!((v3proto_num)==(-1))) { goto L4; }
   value0 = cl_error(3, VV[46], ECL_SYM("NAME",1300), v2string);
   return value0;
L4:;
   value0 = ecl_make_fixnum(v3proto_num);
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      function definition for MAKE-INET-ADDRESS                     */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L20make_inet_address(cl_object v1dotted_quads)
{
 cl_object T0, T1;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = (ECL_SYM("PARSE-INTEGER",630)->symbol.gfdef);
  T1 = L5split(3, v1dotted_quads, ECL_NIL, VV[49]);
  value0 = cl_map(3, ECL_SYM("VECTOR",900), T0, T1);
  return value0;
 }
}
/*      local function LAMBDA72                                       */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC21__lambda72()
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  value0 = ecl_symbol_value(VV[0]);
  cl_env_copy->nvalues = 1;
  return value0;
 }
}
/*      function definition for MAKE-INET-SOCKET                      */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L22make_inet_socket(cl_object v1type, cl_object v2protocol)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  value0 = (cl_env_copy->function=(ECL_SYM("MAKE-INSTANCE",951)->symbol.gfdef))->cfun.entry(5, VV[51], ECL_SYM("TYPE",1346), v1type, VV[52], v2protocol) /*  MAKE-INSTANCE */;
  return value0;
 }
}
/*      local function SOCKET-BIND                                    */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC23socket_bind(cl_narg narg, cl_object v1socket, ...)
{
 cl_object T0, T1, T2, T3, T4;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 if (ecl_unlikely(narg<1)) FEwrong_num_arguments_anonym();
 {
  cl_object v2address;
  ecl_va_list args; ecl_va_start(args,v1socket,narg,1);
  v2address = cl_grab_rest_args(args);
  ecl_va_end(args);
  goto L3;
L2:;
  {
   cl_object v3;
   T0 = ecl_list1(v2address);
   v3 = si_assert_failure(4, VV[54], VV[55], T0, VV[56]);
   v2address = v3;
  }
L3:;
  {
   cl_fixnum v3;
   v3 = ecl_length(v2address);
   if ((2)==(v3)) { goto L9; }
  }
  goto L2;
L9:;
  {
   cl_object v3;
   cl_object v4;
   cl_object v5port;
   v3 = ecl_car(v2address);
   v4 = ecl_cadr(v2address);
   v5port = v4;
   {
    cl_fixnum v6;
    {
     cl_object v7;
     v7 = v3;
     T1 = v7;
     {
      cl_fixnum v8;
      if (ecl_unlikely((0)>=(v7)->vector.dim))
           FEwrong_index(ECL_NIL,v7,-1,ecl_make_fixnum(0),(v7)->vector.dim);
      value0 = ECL_NIL;
      cl_env_copy->nvalues = 0;
      v8 = 0;
      T0 = ecl_aref_unsafe(T1,v8);
     }
    }
    {
     cl_object v7;
     v7 = v3;
     T2 = v7;
     {
      cl_fixnum v8;
      if (ecl_unlikely((1)>=(v7)->vector.dim))
           FEwrong_index(ECL_NIL,v7,-1,ecl_make_fixnum(1),(v7)->vector.dim);
      value0 = ECL_NIL;
      cl_env_copy->nvalues = 0;
      v8 = 1;
      T1 = ecl_aref_unsafe(T2,v8);
     }
    }
    {
     cl_object v7;
     v7 = v3;
     T3 = v7;
     {
      cl_fixnum v8;
      if (ecl_unlikely((2)>=(v7)->vector.dim))
           FEwrong_index(ECL_NIL,v7,-1,ecl_make_fixnum(2),(v7)->vector.dim);
      value0 = ECL_NIL;
      cl_env_copy->nvalues = 0;
      v8 = 2;
      T2 = ecl_aref_unsafe(T3,v8);
     }
    }
    {
     cl_object v7;
     v7 = v3;
     T4 = v7;
     {
      cl_fixnum v8;
      if (ecl_unlikely((3)>=(v7)->vector.dim))
           FEwrong_index(ECL_NIL,v7,-1,ecl_make_fixnum(3),(v7)->vector.dim);
      value0 = ECL_NIL;
      cl_env_copy->nvalues = 0;
      v8 = 3;
      T3 = ecl_aref_unsafe(T4,v8);
     }
    }
    T4 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
    {
     int v7;
     
{
        struct sockaddr_in sockaddr;
        int output;
        ecl_disable_interrupts();
        fill_inet_sockaddr(&sockaddr, ecl_to_int(v5port), ecl_to_int(T0), ecl_to_int(T1), ecl_to_int(T2), ecl_to_int(T3));
        output = bind(ecl_to_int(T4),(struct sockaddr*)&sockaddr, sizeof(struct sockaddr_in));
        ecl_enable_interrupts();
        v7= output;
}
     v6 = (cl_fixnum)(v7);
    }
    if (!((-1)==(v6))) { goto L14; }
   }
   value0 = L45socket_error(VV[57]);
   return value0;
L14:;
   value0 = ECL_NIL;
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      local function SOCKET-ACCEPT                                  */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC24socket_accept(cl_object v1socket)
{
 cl_object T0, T1, T2, T3;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v2sfd;
   v2sfd = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   {
    cl_object v4;                                 /*  FD              */
    cl_object v5;                                 /*  VECTOR          */
    cl_object v6;                                 /*  PORT            */
    {
     int v7;
     cl_object v8;
     int v9;
     {
        struct sockaddr_in sockaddr;
        socklen_t addr_len = (socklen_t)sizeof(struct sockaddr_in);
        int new_fd;

        ecl_disable_interrupts();
        new_fd = accept(ecl_to_int(v2sfd), (struct sockaddr*)&sockaddr, &addr_len);
        ecl_enable_interrupts();

        v7= new_fd;
        v8= ECL_NIL;
        v9= 0;
        if (new_fd != -1) {
                uint32_t ip = ntohl(sockaddr.sin_addr.s_addr);
                uint16_t port = ntohs(sockaddr.sin_port);
                cl_object vector = cl_make_array(1,ecl_make_fixnum(4));

                ecl_aset(vector,0, ecl_make_fixnum( ip>>24 ));
                ecl_aset(vector,1, ecl_make_fixnum( (ip>>16) & 0xFF));
                ecl_aset(vector,2, ecl_make_fixnum( (ip>>8) & 0xFF));
                ecl_aset(vector,3, ecl_make_fixnum( ip & 0xFF ));

                v8= vector;
                v9= port;
        }
}
     cl_env_copy->values[0] = ecl_make_int(v7);
     cl_env_copy->values[1] = v8;
     cl_env_copy->values[2] = ecl_make_int(v9);cl_env_copy->nvalues = 3;
     value0 = cl_env_copy->values[0];
    }
    {
     v4 = value0;
     v5 = cl_env_copy->values[1];
     v6 = cl_env_copy->values[2];
    }
    if (!((ecl_fixnum(v4))==(-1))) { goto L3; }
    value0 = L45socket_error(VV[58]);
    return value0;
L3:;
    T0 = cl_class_of(v1socket);
    T1 = ecl_function_dispatch(cl_env_copy,VV[179])(1, v1socket) /*  SOCKET-TYPE */;
    T2 = ecl_function_dispatch(cl_env_copy,VV[190])(1, v1socket) /*  SOCKET-PROTOCOL */;
    T3 = (cl_env_copy->function=(ECL_SYM("MAKE-INSTANCE",951)->symbol.gfdef))->cfun.entry(7, T0, ECL_SYM("TYPE",1346), T1, VV[52], T2, VV[59], v4) /*  MAKE-INSTANCE */;
    cl_env_copy->nvalues = 3;
    cl_env_copy->values[2] = v6;
    cl_env_copy->values[1] = v5;
    cl_env_copy->values[0] = T3;
    return cl_env_copy->values[0];
   }
  }
 }
}
/*      local function SOCKET-CONNECT                                 */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC25socket_connect(cl_narg narg, cl_object v1socket, ...)
{
 cl_object T0, T1, T2, T3, T4;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 if (ecl_unlikely(narg<1)) FEwrong_num_arguments_anonym();
 {
  cl_object v2address;
  ecl_va_list args; ecl_va_start(args,v1socket,narg,1);
  v2address = cl_grab_rest_args(args);
  ecl_va_end(args);
  {
   cl_object v3;
   cl_object v4;
   cl_object v5port;
   v3 = ecl_car(v2address);
   v4 = ecl_cadr(v2address);
   v5port = v4;
   {
    cl_fixnum v6;
    {
     cl_object v7;
     v7 = v3;
     T1 = v7;
     {
      cl_fixnum v8;
      if (ecl_unlikely((0)>=(v7)->vector.dim))
           FEwrong_index(ECL_NIL,v7,-1,ecl_make_fixnum(0),(v7)->vector.dim);
      value0 = ECL_NIL;
      cl_env_copy->nvalues = 0;
      v8 = 0;
      T0 = ecl_aref_unsafe(T1,v8);
     }
    }
    {
     cl_object v7;
     v7 = v3;
     T2 = v7;
     {
      cl_fixnum v8;
      if (ecl_unlikely((1)>=(v7)->vector.dim))
           FEwrong_index(ECL_NIL,v7,-1,ecl_make_fixnum(1),(v7)->vector.dim);
      value0 = ECL_NIL;
      cl_env_copy->nvalues = 0;
      v8 = 1;
      T1 = ecl_aref_unsafe(T2,v8);
     }
    }
    {
     cl_object v7;
     v7 = v3;
     T3 = v7;
     {
      cl_fixnum v8;
      if (ecl_unlikely((2)>=(v7)->vector.dim))
           FEwrong_index(ECL_NIL,v7,-1,ecl_make_fixnum(2),(v7)->vector.dim);
      value0 = ECL_NIL;
      cl_env_copy->nvalues = 0;
      v8 = 2;
      T2 = ecl_aref_unsafe(T3,v8);
     }
    }
    {
     cl_object v7;
     v7 = v3;
     T4 = v7;
     {
      cl_fixnum v8;
      if (ecl_unlikely((3)>=(v7)->vector.dim))
           FEwrong_index(ECL_NIL,v7,-1,ecl_make_fixnum(3),(v7)->vector.dim);
      value0 = ECL_NIL;
      cl_env_copy->nvalues = 0;
      v8 = 3;
      T3 = ecl_aref_unsafe(T4,v8);
     }
    }
    T4 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
    {
     int v7;
     
{
        struct sockaddr_in sockaddr;
        int output;

        ecl_disable_interrupts();
        fill_inet_sockaddr(&sockaddr, ecl_to_int(v5port), ecl_to_int(T0), ecl_to_int(T1), ecl_to_int(T2), ecl_to_int(T3));
        output = connect(ecl_to_int(T4),(struct sockaddr*)&sockaddr, sizeof(struct sockaddr_in));
        ecl_enable_interrupts();

        v7= output;
}
     v6 = (cl_fixnum)(v7);
    }
    if (!((-1)==(v6))) { goto L4; }
   }
   value0 = L45socket_error(VV[60]);
   return value0;
L4:;
   value0 = ECL_NIL;
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      local function SOCKET-PEERNAME                                */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC26socket_peername(cl_object v1socket)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v2vector;
   cl_object v3fd;
   cl_fixnum v4port;
   v2vector = si_make_vector(ECL_T, ecl_make_fixnum(4), ECL_NIL, ECL_NIL, ECL_NIL, ecl_make_fixnum(0));
   v3fd = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   {
    int v5;
    {
        struct sockaddr_in name;
        socklen_t len = sizeof(struct sockaddr_in);
        int ret;

        ecl_disable_interrupts();
        ret = getpeername(ecl_to_int(v3fd),(struct sockaddr*)&name,&len);
        ecl_enable_interrupts();

        if (ret == 0) {
                uint32_t ip = ntohl(name.sin_addr.s_addr);
                uint16_t port = ntohs(name.sin_port);

                ecl_aset(v2vector,0, ecl_make_fixnum( ip>>24 ));
                ecl_aset(v2vector,1, ecl_make_fixnum( (ip>>16) & 0xFF));
                ecl_aset(v2vector,2, ecl_make_fixnum( (ip>>8) & 0xFF));
                ecl_aset(v2vector,3, ecl_make_fixnum( ip & 0xFF ));

                v5= port;
         } else {
                v5= -1;
         }
}
    v4port = (cl_fixnum)(v5);
   }
   if (!((v4port)>=(0))) { goto L4; }
   cl_env_copy->nvalues = 2;
   cl_env_copy->values[1] = ecl_make_fixnum(v4port);
   cl_env_copy->values[0] = v2vector;
   return cl_env_copy->values[0];
L4:;
   value0 = L45socket_error(VV[61]);
   return value0;
  }
 }
}
/*      local function SOCKET-NAME                                    */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC27socket_name(cl_object v1socket)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v2vector;
   cl_object v3fd;
   cl_fixnum v4port;
   v2vector = si_make_vector(ECL_T, ecl_make_fixnum(4), ECL_NIL, ECL_NIL, ECL_NIL, ecl_make_fixnum(0));
   v3fd = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   {
    int v5;
    {
        struct sockaddr_in name;
        socklen_t len = sizeof(struct sockaddr_in);
        int ret;

        ecl_disable_interrupts();
        ret = getsockname(ecl_to_int(v3fd),(struct sockaddr*)&name,&len);
        ecl_enable_interrupts();

        if (ret == 0) {
                uint32_t ip = ntohl(name.sin_addr.s_addr);
                uint16_t port = ntohs(name.sin_port);

                ecl_aset(v2vector,0, ecl_make_fixnum( ip>>24 ));
                ecl_aset(v2vector,1, ecl_make_fixnum( (ip>>16) & 0xFF));
                ecl_aset(v2vector,2, ecl_make_fixnum( (ip>>8) & 0xFF));
                ecl_aset(v2vector,3, ecl_make_fixnum( ip & 0xFF ));

                v5= port;
         } else {
                v5= -1;
         }
}
    v4port = (cl_fixnum)(v5);
   }
   if (!((v4port)>=(0))) { goto L4; }
   cl_env_copy->nvalues = 2;
   cl_env_copy->values[1] = ecl_make_fixnum(v4port);
   cl_env_copy->values[0] = v2vector;
   return cl_env_copy->values[0];
L4:;
   value0 = L45socket_error(VV[62]);
   return value0;
  }
 }
}
/*      local function SOCKET-SEND                                    */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC28socket_send(cl_narg narg, cl_object v1socket, cl_object v2buffer, cl_object v3length, ...)
{
 cl_object T0, T1, T2, T3, T4, T5;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 if (ecl_unlikely(narg<3)) FEwrong_num_arguments_anonym();
 {
  cl_object v4address;
  cl_object v5external_format;
  cl_object v6oob;
  cl_object v7eor;
  cl_object v8dontroute;
  cl_object v9dontwait;
  cl_object v10nosignal;
  cl_object v11confirm;
  cl_object v12more;
  ecl_va_list args; ecl_va_start(args,v3length,narg,3);
  {
   cl_object keyvars[18];
   cl_parse_key(args,9,LC28socket_sendkeys,keyvars,NULL,TRUE);
   ecl_va_end(args);
   v4address = keyvars[0];
   v5external_format = keyvars[1];
   v6oob = keyvars[2];
   v7eor = keyvars[3];
   v8dontroute = keyvars[4];
   v9dontwait = keyvars[5];
   v10nosignal = keyvars[6];
   v11confirm = keyvars[7];
   v12more = keyvars[8];
  }
  goto L3;
L2:;
  si_assert_failure(1, VV[63]);
L3:;
  if (ECL_STRINGP(v2buffer)) { goto L6; }
  if (ECL_VECTORP(v2buffer)) { goto L6; }
  goto L2;
L6:;
  {
   cl_object v13;
   cl_object v14;
   cl_object v15length;
   cl_object v16fd;
   value0 = v3length;
   if ((value0)!=ECL_NIL) { goto L11; }
   v13 = ecl_make_fixnum(ecl_length(v2buffer));
   goto L9;
L11:;
   v13 = value0;
   goto L9;
L9:;
   v14 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   v15length = v13;
   v16fd = v14;
   {
    long v17len_sent;
    if (Null(v4address)) { goto L17; }
    goto L21;
L20:;
    si_assert_failure(1, VV[54]);
L21:;
    {
     cl_fixnum v18;
     v18 = ecl_length(v4address);
     if ((2)==(v18)) { goto L24; }
    }
    goto L20;
L24:;
    T0 = ecl_cadr(v4address);
    {
     cl_object v18;
     v18 = ecl_car(v4address);
     T2 = v18;
     {
      cl_fixnum v19;
      if (ecl_unlikely((0)>=(v18)->vector.dim))
           FEwrong_index(ECL_NIL,v18,-1,ecl_make_fixnum(0),(v18)->vector.dim);
      value0 = ECL_NIL;
      cl_env_copy->nvalues = 0;
      v19 = 0;
      T1 = ecl_aref_unsafe(T2,v19);
     }
    }
    {
     cl_object v18;
     v18 = ecl_car(v4address);
     T3 = v18;
     {
      cl_fixnum v19;
      if (ecl_unlikely((1)>=(v18)->vector.dim))
           FEwrong_index(ECL_NIL,v18,-1,ecl_make_fixnum(1),(v18)->vector.dim);
      value0 = ECL_NIL;
      cl_env_copy->nvalues = 0;
      v19 = 1;
      T2 = ecl_aref_unsafe(T3,v19);
     }
    }
    {
     cl_object v18;
     v18 = ecl_car(v4address);
     T4 = v18;
     {
      cl_fixnum v19;
      if (ecl_unlikely((2)>=(v18)->vector.dim))
           FEwrong_index(ECL_NIL,v18,-1,ecl_make_fixnum(2),(v18)->vector.dim);
      value0 = ECL_NIL;
      cl_env_copy->nvalues = 0;
      v19 = 2;
      T3 = ecl_aref_unsafe(T4,v19);
     }
    }
    {
     cl_object v18;
     v18 = ecl_car(v4address);
     T5 = v18;
     {
      cl_fixnum v19;
      if (ecl_unlikely((3)>=(v18)->vector.dim))
           FEwrong_index(ECL_NIL,v18,-1,ecl_make_fixnum(3),(v18)->vector.dim);
      value0 = ECL_NIL;
      cl_env_copy->nvalues = 0;
      v19 = 3;
      T4 = ecl_aref_unsafe(T5,v19);
     }
    }
    {
     long v18;
     
{
        int sock = ecl_to_int(v16fd);
        int length = ecl_to_int(v15length);
        void *buffer = safe_buffer_pointer(v2buffer, length);
        int flags = ( (v6oob)!=ECL_NIL ? MSG_OOB : 0 )  |
                    ( (v7eor)!=ECL_NIL ? MSG_EOR : 0 ) |
                    ( (v8dontroute)!=ECL_NIL ? MSG_DONTROUTE : 0 ) |
                    ( (v9dontwait)!=ECL_NIL ? MSG_DONTWAIT : 0 ) |
                    ( (v10nosignal)!=ECL_NIL ? MSG_NOSIGNAL : 0 ) |
                    ( (v11confirm)!=ECL_NIL ? MSG_CONFIRM : 0 );
        cl_type type = ecl_t_of(v2buffer);
        struct sockaddr_in sockaddr;
        ssize_t len;

        ecl_disable_interrupts();
        fill_inet_sockaddr(&sockaddr, ecl_to_int(T0), ecl_to_int(T1), ecl_to_int(T2), ecl_to_int(T3), ecl_to_int(T4));
#if (MSG_NOSIGNAL == 0) && defined(SO_NOSIGPIPE)
        {
                int sockopt = (v10nosignal)!=ECL_NIL;
                setsockopt(ecl_to_int(v16fd),SOL_SOCKET,SO_NOSIGPIPE,
                           wincoerce(char *,&sockopt),
                           sizeof(int));
        }
#endif
        len = sendto(sock, wincoerce(char *,buffer),
                     length, flags,(struct sockaddr*)&sockaddr, 
                     sizeof(struct sockaddr_in));
        ecl_enable_interrupts();
        v18= len;
}

     v17len_sent = v18;
     goto L16;
    }
L17:;
    {
     long v19;
     
{
        int sock = ecl_to_int(v16fd);
        int length = ecl_to_int(v15length);
        void *buffer = safe_buffer_pointer(v2buffer, length);
        int flags = ( (v6oob)!=ECL_NIL ? MSG_OOB : 0 )  |
                    ( (v7eor)!=ECL_NIL ? MSG_EOR : 0 ) |
                    ( (v8dontroute)!=ECL_NIL ? MSG_DONTROUTE : 0 ) |
                    ( (v9dontwait)!=ECL_NIL ? MSG_DONTWAIT : 0 ) |
                    ( (v10nosignal)!=ECL_NIL ? MSG_NOSIGNAL : 0 ) |
                    ( (v11confirm)!=ECL_NIL ? MSG_CONFIRM : 0 );
        cl_type type = ecl_t_of(v2buffer);
        ssize_t len;
        ecl_disable_interrupts();
#if (MSG_NOSIGNAL == 0) && defined(SO_NOSIGPIPE)
        {
                int sockopt = (v10nosignal)!=ECL_NIL;
                setsockopt(ecl_to_int(v16fd),SOL_SOCKET,SO_NOSIGPIPE,
                           wincoerce(char *,&sockopt),
                           sizeof(int));
        }
#endif
        len = send(sock, wincoerce(char *, buffer), length, flags);
        ecl_enable_interrupts();
        v19= len;
}

     v17len_sent = v19;
    }
L16:;
    if (!(ecl_number_equalp(ecl_make_long(v17len_sent),ecl_make_fixnum(-1)))) { goto L46; }
    value0 = L45socket_error(VV[64]);
    return value0;
L46:;
    value0 = ecl_make_long(v17len_sent);
    cl_env_copy->nvalues = 1;
    return value0;
   }
  }
 }
}
/*      local function LAMBDA88                                       */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC29__lambda88()
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  value0 = ecl_symbol_value(VV[1]);
  cl_env_copy->nvalues = 1;
  return value0;
 }
}
/*      local function SOCKET-BIND                                    */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC30socket_bind(cl_narg narg, cl_object v1socket, ...)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 if (ecl_unlikely(narg<1)) FEwrong_num_arguments_anonym();
 {
  cl_object v2address;
  ecl_va_list args; ecl_va_start(args,v1socket,narg,1);
  v2address = cl_grab_rest_args(args);
  ecl_va_end(args);
  goto L3;
L2:;
  {
   cl_object v3;
   T0 = ecl_list1(v2address);
   v3 = si_assert_failure(4, VV[66], VV[55], T0, VV[67]);
   v2address = v3;
  }
L3:;
  {
   cl_fixnum v3;
   v3 = ecl_length(v2address);
   if ((1)==(v3)) { goto L9; }
  }
  goto L2;
L9:;
  {
   cl_object v3;
   cl_object v4;
   cl_object v5;
   cl_object v6fd;
   cl_object v7family;
   v3 = ecl_car(v2address);
   v4 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   v5 = ecl_function_dispatch(cl_env_copy,VV[178])(1, v1socket) /*  SOCKET-FAMILY */;
   v6fd = v4;
   v7family = v5;
   {
    cl_fixnum v8;
    {
     cl_object v9;
     v9 = si_copy_to_simple_base_string(v3);
     {
      int v10;
      
{
        struct sockaddr_un sockaddr;
        size_t size;
        int output;
#ifdef BSD
        sockaddr.sun_len = sizeof(struct sockaddr_un);
#endif
        sockaddr.sun_family = ecl_to_int(v7family);
        strncpy(sockaddr.sun_path,ecl_base_string_pointer_safe(v9),sizeof(sockaddr.sun_path));
        sockaddr.sun_path[sizeof(sockaddr.sun_path)-1] = 0;

        ecl_disable_interrupts();
        output = bind(ecl_to_int(v6fd),(struct sockaddr*)&sockaddr, sizeof(struct sockaddr_un));
        ecl_enable_interrupts();

        v10= output;
}
      v8 = (cl_fixnum)(v10);
     }
    }
    if (!((-1)==(v8))) { goto L16; }
   }
   value0 = L45socket_error(VV[57]);
   return value0;
L16:;
   value0 = ECL_NIL;
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      local function SOCKET-ACCEPT                                  */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC31socket_accept(cl_object v1socket)
{
 cl_object T0, T1, T2, T3;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v3;                                  /*  FD              */
   cl_object v4;                                  /*  NAME            */
   T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   {
    int v5;
    cl_object v6;
    {
        struct sockaddr_un sockaddr;
        socklen_t addr_len = (socklen_t)sizeof(struct sockaddr_un);
        int new_fd;
        ecl_disable_interrupts();
        new_fd = accept(ecl_to_int(T0), (struct sockaddr *)&sockaddr, &addr_len);
        ecl_enable_interrupts();
        v5= new_fd;
        v6= (new_fd == -1) ? ECL_NIL : ecl_make_simple_base_string(sockaddr.sun_path,-1);
}
    cl_env_copy->values[0] = ecl_make_int(v5);
    cl_env_copy->values[1] = v6;cl_env_copy->nvalues = 2;
    value0 = cl_env_copy->values[0];
   }
   {
    v3 = value0;
    v4 = cl_env_copy->values[1];
   }
   if (!((ecl_fixnum(v3))==(-1))) { goto L2; }
   value0 = L45socket_error(VV[58]);
   return value0;
L2:;
   T0 = cl_class_of(v1socket);
   T1 = ecl_function_dispatch(cl_env_copy,VV[179])(1, v1socket) /*  SOCKET-TYPE */;
   T2 = ecl_function_dispatch(cl_env_copy,VV[190])(1, v1socket) /*  SOCKET-PROTOCOL */;
   T3 = (cl_env_copy->function=(ECL_SYM("MAKE-INSTANCE",951)->symbol.gfdef))->cfun.entry(7, T0, ECL_SYM("TYPE",1346), T1, VV[52], T2, VV[59], v3) /*  MAKE-INSTANCE */;
   cl_env_copy->nvalues = 2;
   cl_env_copy->values[1] = v4;
   cl_env_copy->values[0] = T3;
   return cl_env_copy->values[0];
  }
 }
}
/*      local function SOCKET-CONNECT                                 */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC32socket_connect(cl_narg narg, cl_object v1socket, ...)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 if (ecl_unlikely(narg<1)) FEwrong_num_arguments_anonym();
 {
  cl_object v2address;
  ecl_va_list args; ecl_va_start(args,v1socket,narg,1);
  v2address = cl_grab_rest_args(args);
  ecl_va_end(args);
  goto L3;
L2:;
  {
   cl_object v3;
   T0 = ecl_list1(v2address);
   v3 = si_assert_failure(4, VV[66], VV[55], T0, VV[68]);
   v2address = v3;
  }
L3:;
  {
   cl_fixnum v3;
   v3 = ecl_length(v2address);
   if ((1)==(v3)) { goto L9; }
  }
  goto L2;
L9:;
  {
   cl_object v3;
   cl_object v4;
   cl_object v5;
   cl_object v6fd;
   cl_object v7family;
   v3 = ecl_car(v2address);
   v4 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   v5 = ecl_function_dispatch(cl_env_copy,VV[178])(1, v1socket) /*  SOCKET-FAMILY */;
   v6fd = v4;
   v7family = v5;
   {
    cl_fixnum v8;
    {
     cl_object v9;
     v9 = si_copy_to_simple_base_string(v3);
     {
      int v10;
      
{
        struct sockaddr_un sockaddr;
        int output;
#ifdef BSD
        sockaddr.sun_len = sizeof(struct sockaddr_un);
#endif
        sockaddr.sun_family = ecl_to_int(v7family);
        strncpy(sockaddr.sun_path,ecl_base_string_pointer_safe(v9),sizeof(sockaddr.sun_path));
        sockaddr.sun_path[sizeof(sockaddr.sun_path)-1] = 0;

        ecl_disable_interrupts();
        output = connect(ecl_to_int(v6fd),(struct sockaddr*)&sockaddr, sizeof(struct sockaddr_un));
        ecl_enable_interrupts();

        v10= output;
}
      v8 = (cl_fixnum)(v10);
     }
    }
    if (!((-1)==(v8))) { goto L16; }
   }
   value0 = L45socket_error(VV[60]);
   return value0;
L16:;
   value0 = ECL_NIL;
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      local function SOCKET-PEERNAME                                */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC33socket_peername(cl_object v1socket)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v2fd;
   cl_object v3peer;
   v2fd = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   {
    cl_object v4;
    
{
        struct sockaddr_un name;
        socklen_t len = sizeof(struct sockaddr_un);
        int ret;

        ecl_disable_interrupts();
        ret = getpeername(ecl_to_int(v2fd),(struct sockaddr*)&name,&len);
        ecl_enable_interrupts();

        if (ret == 0) {
                v4= ecl_make_simple_base_string(name.sun_path,-1);
        } else {
                v4= ECL_NIL;
        }
}
    v3peer = v4;
   }
   if (Null(v3peer)) { goto L3; }
   value0 = v3peer;
   cl_env_copy->nvalues = 1;
   return value0;
L3:;
   value0 = L45socket_error(VV[61]);
   return value0;
  }
 }
}
/*      local function NON-BLOCKING-MODE                              */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC34non_blocking_mode(cl_object v1socket)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v2fd;
   v2fd = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   {
    cl_fixnum v3;
    v3 = (cl_fixnum)(fcntl(ecl_to_int(v2fd),F_GETFL,NULL)&O_NONBLOCK);
    {
     bool v4;
     v4 = (v3)==0;
     value0 = (v4)?ECL_NIL:ECL_T;
     cl_env_copy->nvalues = 1;
     return value0;
    }
   }
  }
 }
}
/*      local function (SETF NON-BLOCKING-MODE)                       */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC35_setf_non_blocking_mode_(cl_object v1non_blocking_p, cl_object v2socket)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v3;
   cl_fixnum v4;
   cl_object v5fd;
   cl_object v6nblock;
   v3 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v2socket) /*  SOCKET-FILE-DESCRIPTOR */;
   if (Null(v1non_blocking_p)) { goto L3; }
   v4 = 1;
   goto L2;
L3:;
   v4 = 0;
L2:;
   v5fd = v3;
   v6nblock = ecl_make_fixnum(v4);
   {
    cl_fixnum v7;
    {
     int v8;
     
{
        int oldflags = fcntl(ecl_to_int(v5fd),F_GETFL,NULL);
        int newflags = (oldflags & ~O_NONBLOCK) |
                       (ecl_to_int(v6nblock) ? O_NONBLOCK : 0);
        ecl_disable_interrupts();
        v8= fcntl(ecl_to_int(v5fd),F_SETFL,newflags);
        ecl_enable_interrupts();
}
     v7 = (cl_fixnum)(v8);
    }
    if (!((-1)==(v7))) { goto L7; }
   }
   value0 = L45socket_error(VV[69]);
   return value0;
L7:;
   value0 = v1non_blocking_p;
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      function definition for DUP                                   */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L36dup(cl_object v1fd)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  value0 = ecl_make_int(dup(ecl_to_int(v1fd)));
  cl_env_copy->nvalues = 1;
  return value0;
 }
}
/*      function definition for AUTO-CLOSE-TWO-WAY-STREAM             */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L37auto_close_two_way_stream(cl_object v1stream)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  (v1stream)->stream.flags |= ECL_STREAM_CLOSE_COMPONENTS;
  value0 = ECL_NIL;
  cl_env_copy->nvalues = 0;
  return value0;
 }
}
/*      function definition for SOCKET-MAKE-STREAM-INNER              */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L38socket_make_stream_inner(cl_object v1fd, cl_object v2input, cl_object v3output, cl_object v4buffering, cl_object v5element_type, cl_object v6external_format)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  if (Null(v2input)) { goto L1; }
  if (Null(v3output)) { goto L1; }
  {
   cl_object v7in;
   cl_object v8out;
   cl_object v9stream;
   T0 = L36dup(v1fd);
   v7in = L38socket_make_stream_inner(T0, ECL_T, ECL_NIL, v4buffering, v5element_type, v6external_format);
   v8out = L38socket_make_stream_inner(v1fd, ECL_NIL, ECL_T, v4buffering, v5element_type, v6external_format);
   v9stream = cl_make_two_way_stream(v7in, v8out);
   L37auto_close_two_way_stream(v9stream);
   value0 = v9stream;
   cl_env_copy->nvalues = 1;
   return value0;
  }
L1:;
  if (Null(v2input)) { goto L8; }
  value0 = ecl_function_dispatch(cl_env_copy,ECL_SYM("MAKE-STREAM-FROM-FD",1076))(8, v1fd, ECL_SYM("INPUT",1280), VV[71], v4buffering, ECL_SYM("ELEMENT-TYPE",1246), v5element_type, ECL_SYM("EXTERNAL-FORMAT",1258), v6external_format) /*  MAKE-STREAM-FROM-FD */;
  return value0;
L8:;
  if (Null(v3output)) { goto L10; }
  value0 = ecl_function_dispatch(cl_env_copy,ECL_SYM("MAKE-STREAM-FROM-FD",1076))(8, v1fd, ECL_SYM("OUTPUT",1309), VV[71], v4buffering, ECL_SYM("ELEMENT-TYPE",1246), v5element_type, ECL_SYM("EXTERNAL-FORMAT",1258), v6external_format) /*  MAKE-STREAM-FROM-FD */;
  return value0;
L10:;
  value0 = cl_error(1, VV[72]);
  return value0;
 }
}
/*      local function SOCKET-MAKE-STREAM                             */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC39socket_make_stream(cl_narg narg, cl_object v1socket, ...)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 if (ecl_unlikely(narg<1)) FEwrong_num_arguments_anonym();
 {
  cl_object v2input;
  cl_object v3input_p;
  cl_object v4output;
  cl_object v5output_p;
  cl_object v6buffering;
  cl_object v7element_type;
  cl_object v8external_format;
  ecl_va_list args; ecl_va_start(args,v1socket,narg,1);
  {
   cl_object keyvars[10];
   cl_parse_key(args,5,LC39socket_make_streamkeys,keyvars,NULL,TRUE);
   ecl_va_end(args);
   v2input = keyvars[0];
   v3input_p = keyvars[5];
   v4output = keyvars[1];
   v5output_p = keyvars[6];
   if (Null(keyvars[7])) {
    v6buffering = ECL_SYM("FULL",1662);
   } else {
    v6buffering = keyvars[2];
   }
   if (Null(keyvars[8])) {
    v7element_type = ECL_SYM("BASE-CHAR",122);
   } else {
    v7element_type = keyvars[3];
   }
   if (Null(keyvars[9])) {
    v8external_format = ECL_SYM("DEFAULT",1237);
   } else {
    v8external_format = keyvars[4];
   }
  }
  {
   cl_object v9stream;
   if (Null(cl_slot_boundp(v1socket, ECL_SYM("STREAM",801)))) { goto L6; }
   v9stream = cl_slot_value(v1socket, ECL_SYM("STREAM",801));
   goto L4;
L6:;
   v9stream = ECL_NIL;
   goto L4;
L4:;
   if ((v9stream)!=ECL_NIL) { goto L8; }
   if ((v3input_p)!=ECL_NIL) { goto L10; }
   if ((v5output_p)!=ECL_NIL) { goto L10; }
   v2input = ECL_T;
   v4output = ECL_T;
L10:;
   T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
   v9stream = L38socket_make_stream_inner(T0, v2input, v4output, v6buffering, v7element_type, v8external_format);
   (cl_env_copy->function=ECL_CONS_CAR(VV[180]))->cfun.entry(3, v9stream, v1socket, ECL_SYM("STREAM",801)) /*  (SETF SLOT-VALUE) */;
L8:;
   value0 = v9stream;
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      local function STREAM-FD                                      */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC40ext__stream_fd(cl_object v1socket)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  value0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
  return value0;
 }
}
/*      local function LAMBDA127                                      */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC42__lambda127()
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC41__lambda128,ECL_NIL,Cblock,2);
   value0 = v1;
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      local function LAMBDA128                                      */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC41__lambda128(cl_object v1c, cl_object v2s)
{
 cl_object T0, T1, T2;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v3num;
   v3num = ecl_function_dispatch(cl_env_copy,VV[207])(1, v1c) /*  SOCKET-ERROR-ERRNO */;
   T0 = ecl_function_dispatch(cl_env_copy,VV[208])(1, v1c) /*  SOCKET-ERROR-SYSCALL */;
   value0 = ecl_function_dispatch(cl_env_copy,VV[79])(1, v1c) /*  SOCKET-ERROR-SYMBOL */;
   if ((value0)!=ECL_NIL) { goto L4; }
   T1 = ecl_function_dispatch(cl_env_copy,VV[207])(1, v1c) /*  SOCKET-ERROR-ERRNO */;
   goto L2;
L4:;
   T1 = value0;
   goto L2;
L2:;
   T2 = ecl_cstring_to_base_string_or_nil(strerror(ecl_to_int(v3num)));
   value0 = cl_format(5, v2s, VV[76], T0, T1, T2);
   return value0;
  }
 }
}
/*      local function DEFINE-SOCKET-CONDITION                        */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC43define_socket_condition(cl_object v1, cl_object v2)
{
 cl_object T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v3;
   cl_object v4symbol;
   cl_object v5name;
   v3 = ecl_cdr(v1);
   if (!(v3==ECL_NIL)) { goto L3; }
   ecl_function_dispatch(cl_env_copy,VV[210])(1, v1) /*  DM-TOO-FEW-ARGUMENTS */;
L3:;
   {
    cl_object v6;
    v6 = ecl_car(v3);
    v3 = ecl_cdr(v3);
    v4symbol = v6;
   }
   if (!(v3==ECL_NIL)) { goto L9; }
   ecl_function_dispatch(cl_env_copy,VV[210])(1, v1) /*  DM-TOO-FEW-ARGUMENTS */;
L9:;
   {
    cl_object v6;
    v6 = ecl_car(v3);
    v3 = ecl_cdr(v3);
    v5name = v6;
   }
   if (Null(v3)) { goto L14; }
   ecl_function_dispatch(cl_env_copy,VV[211])(1, v1) /*  DM-TOO-MANY-ARGUMENTS */;
L14:;
   T0 = ecl_symbol_name(v4symbol);
   T1 = cl_list(2, VV[77], T0);
   T2 = cl_list(3, ECL_SYM("DEFCONSTANT",279), v4symbol, T1);
   T3 = cl_list(2, ECL_SYM("QUOTE",681), v4symbol);
   T4 = cl_list(5, ECL_SYM("SYMBOL",842), ECL_SYM("READER",1008), VV[79], ECL_SYM("INITFORM",998), T3);
   T5 = ecl_list1(T4);
   T6 = cl_list(4, ECL_SYM("DEFINE-CONDITION",280), v5name, VV[78], T5);
   T7 = cl_list(2, ECL_SYM("QUOTE",681), v5name);
   T8 = cl_list(2, ECL_SYM("EXPORT",346), T7);
   T9 = cl_list(2, ECL_SYM("QUOTE",681), v5name);
   T10 = cl_list(3, ECL_SYM("CONS",253), v4symbol, T9);
   T11 = cl_list(3, ECL_SYM("PUSH",679), T10, VV[80]);
   value0 = cl_list(6, ECL_SYM("LET",479), ECL_NIL, T2, T6, T8, T11);
   return value0;
  }
 }
}
/*      function definition for CONDITION-FOR-ERRNO                   */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L44condition_for_errno(cl_object v1err)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_assql(v1err,ecl_symbol_value(VV[80]));
  value0 = ecl_cdr(T0);
  if ((value0)!=ECL_NIL) { goto L2; }
  value0 = VV[75];
  cl_env_copy->nvalues = 1;
  return value0;
L2:;
  cl_env_copy->nvalues = 1;
  return value0;
 }
}
/*      function definition for SOCKET-ERROR                          */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L45socket_error(cl_object v1where)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_fixnum v2errno;
   cl_object v3condition;
   v2errno = (cl_fixnum)(errno);
   v3condition = L44condition_for_errno(ecl_make_fixnum(v2errno));
   value0 = cl_error(5, v3condition, VV[110], ecl_make_fixnum(v2errno), VV[111], v1where);
   return value0;
  }
 }
}
/*      function definition for NAME-SERVICE-ERROR                    */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L46name_service_error(cl_narg narg, cl_object v1where, ...)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 if (ecl_unlikely(narg<1)) FEwrong_num_arguments_anonym();
 if (ecl_unlikely(narg>2)) FEwrong_num_arguments_anonym();
 {
  cl_object v2errno;
  va_list args; va_start(args,v1where);
  {
   int i = 1;
   if (i >= narg) {
    v2errno = ECL_NIL;
   } else {
    i++;
    v2errno = va_arg(args,cl_object);
   }
  }
  va_end(args);
  {
   cl_fixnum v3;
   v3 = (cl_fixnum)(EAI_SYSTEM);
   if (!(ecl_number_equalp(v2errno,ecl_make_fixnum(v3)))) { goto L2; }
  }
  value0 = L45socket_error(v1where);
  return value0;
L2:;
  {
   cl_object v3condition;
   v3condition = L50condition_for_name_service_errno(v2errno);
   value0 = cl_error(5, v3condition, VV[110], v2errno, VV[111], v1where);
   return value0;
  }
 }
}
/*      local function LAMBDA151                                      */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC48__lambda151()
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC47__lambda152,ECL_NIL,Cblock,2);
   value0 = v1;
   cl_env_copy->nvalues = 1;
   return value0;
  }
 }
}
/*      local function LAMBDA152                                      */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC47__lambda152(cl_object v1c, cl_object v2s)
{
 cl_object T0, T1, T2;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v3num;
   v3num = ecl_function_dispatch(cl_env_copy,VV[215])(1, v1c) /*  NAME-SERVICE-ERROR-ERRNO */;
   T0 = ecl_function_dispatch(cl_env_copy,VV[216])(1, v1c) /*  NAME-SERVICE-ERROR-SYSCALL */;
   value0 = ecl_function_dispatch(cl_env_copy,VV[115])(1, v1c) /*  NAME-SERVICE-ERROR-SYMBOL */;
   if ((value0)!=ECL_NIL) { goto L4; }
   T1 = ecl_function_dispatch(cl_env_copy,VV[215])(1, v1c) /*  NAME-SERVICE-ERROR-ERRNO */;
   goto L2;
L4:;
   T1 = value0;
   goto L2;
L2:;
   T2 = L51get_name_service_error_message(v3num);
   value0 = cl_format(5, v2s, VV[113], T0, T1, T2);
   return value0;
  }
 }
}
/*      local function DEFINE-NAME-SERVICE-CONDITION                  */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC49define_name_service_condition(cl_object v1, cl_object v2)
{
 cl_object T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v3;
   cl_object v4symbol;
   cl_object v5name;
   v3 = ecl_cdr(v1);
   if (!(v3==ECL_NIL)) { goto L3; }
   ecl_function_dispatch(cl_env_copy,VV[210])(1, v1) /*  DM-TOO-FEW-ARGUMENTS */;
L3:;
   {
    cl_object v6;
    v6 = ecl_car(v3);
    v3 = ecl_cdr(v3);
    v4symbol = v6;
   }
   if (!(v3==ECL_NIL)) { goto L9; }
   ecl_function_dispatch(cl_env_copy,VV[210])(1, v1) /*  DM-TOO-FEW-ARGUMENTS */;
L9:;
   {
    cl_object v6;
    v6 = ecl_car(v3);
    v3 = ecl_cdr(v3);
    v5name = v6;
   }
   if (Null(v3)) { goto L14; }
   ecl_function_dispatch(cl_env_copy,VV[211])(1, v1) /*  DM-TOO-MANY-ARGUMENTS */;
L14:;
   T0 = ecl_symbol_name(v4symbol);
   T1 = cl_list(2, VV[77], T0);
   T2 = cl_list(3, ECL_SYM("DEFCONSTANT",279), v4symbol, T1);
   T3 = cl_list(2, ECL_SYM("QUOTE",681), v4symbol);
   T4 = cl_list(5, ECL_SYM("SYMBOL",842), ECL_SYM("READER",1008), VV[115], ECL_SYM("INITFORM",998), T3);
   T5 = ecl_list1(T4);
   T6 = cl_list(4, ECL_SYM("DEFINE-CONDITION",280), v5name, VV[114], T5);
   T7 = cl_list(2, ECL_SYM("QUOTE",681), v5name);
   T8 = cl_list(3, ECL_SYM("CONS",253), v4symbol, T7);
   T9 = cl_list(3, ECL_SYM("PUSH",679), T8, VV[116]);
   T10 = cl_list(2, ECL_SYM("QUOTE",681), v4symbol);
   T11 = cl_list(2, ECL_SYM("EXPORT",346), T10);
   value0 = cl_list(6, ECL_SYM("LET",479), ECL_NIL, T2, T6, T9, T11);
   return value0;
  }
 }
}
/*      function definition for CONDITION-FOR-NAME-SERVICE-ERRNO      */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L50condition_for_name_service_errno(cl_object v1err)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_assql(v1err,ecl_symbol_value(VV[116]));
  value0 = ecl_cdr(T0);
  if ((value0)!=ECL_NIL) { goto L2; }
  value0 = VV[124];
  cl_env_copy->nvalues = 1;
  return value0;
L2:;
  cl_env_copy->nvalues = 1;
  return value0;
 }
}
/*      function definition for GET-NAME-SERVICE-ERROR-MESSAGE        */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L51get_name_service_error_message(cl_object v1num)
{
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  value0 = ecl_cstring_to_base_string_or_nil(gai_strerror(ecl_to_int(v1num)));
  cl_env_copy->nvalues = 1;
  return value0;
 }
}
/*      function definition for GET-SOCKOPT-INT                       */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L52get_sockopt_int(cl_object v1fd, cl_object v2level, cl_object v3const)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v4ret;
   {
    cl_object v5;
    {
        int sockopt, ret;
        socklen_t socklen = sizeof(int);

        ecl_disable_interrupts();
        ret = getsockopt(ecl_to_int(v1fd),ecl_to_int(v2level),ecl_to_int(v3const),wincoerce(char*,&sockopt),&socklen);
        ecl_enable_interrupts();

        v5= (ret == 0) ? ecl_make_integer(sockopt) : ECL_NIL;
}
    v4ret = v5;
   }
   if (Null(v4ret)) { goto L2; }
   value0 = v4ret;
   cl_env_copy->nvalues = 1;
   return value0;
L2:;
   T0 = ecl_cstring_to_base_string_or_nil(strerror(errno));
   value0 = cl_error(2, VV[127], T0);
   return value0;
  }
 }
}
/*      function definition for GET-SOCKOPT-BOOL                      */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L53get_sockopt_bool(cl_object v1fd, cl_object v2level, cl_object v3const)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v4ret;
   {
    cl_object v5;
    {
        int sockopt, ret;
        socklen_t socklen = sizeof(int);

        ecl_disable_interrupts();
        ret = getsockopt(ecl_to_int(v1fd),ecl_to_int(v2level),ecl_to_int(v3const),wincoerce(char*,&sockopt),&socklen);
        ecl_enable_interrupts();

        v5= (ret == 0) ? ecl_make_integer(sockopt) : ECL_NIL;
}
    v4ret = v5;
   }
   if (Null(v4ret)) { goto L2; }
   value0 = ecl_make_bool(!ecl_number_equalp(v4ret,ecl_make_fixnum(0)));
   cl_env_copy->nvalues = 1;
   return value0;
L2:;
   T0 = ecl_cstring_to_base_string_or_nil(strerror(errno));
   value0 = cl_error(2, VV[127], T0);
   return value0;
  }
 }
}
/*      function definition for GET-SOCKOPT-TIMEVAL                   */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L54get_sockopt_timeval(cl_object v1fd, cl_object v2level, cl_object v3const)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v4ret;
   {
    cl_object v5;
    {
        struct timeval tv;
        socklen_t socklen = sizeof(struct timeval);
        int ret;

        ecl_disable_interrupts();
        ret = getsockopt(ecl_to_int(v1fd),ecl_to_int(v2level),ecl_to_int(v3const),wincoerce(char*,&tv),&socklen);
        ecl_enable_interrupts();

        v5= (ret == 0) ? ecl_make_double_float((double)tv.tv_sec
                                        + ((double)tv.tv_usec) / 1000000.0) : ECL_NIL;
}
    v4ret = v5;
   }
   if (Null(v4ret)) { goto L2; }
   value0 = v4ret;
   cl_env_copy->nvalues = 1;
   return value0;
L2:;
   T0 = ecl_cstring_to_base_string_or_nil(strerror(errno));
   value0 = cl_error(2, VV[127], T0);
   return value0;
  }
 }
}
/*      function definition for GET-SOCKOPT-LINGER                    */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L55get_sockopt_linger(cl_object v1fd, cl_object v2level, cl_object v3const)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v4ret;
   {
    cl_object v5;
    {
        struct linger sockopt;
        socklen_t socklen = sizeof(struct linger);
        int ret;

        ecl_disable_interrupts();
        ret = getsockopt(ecl_to_int(v1fd),ecl_to_int(v2level),ecl_to_int(v3const),wincoerce(char*,&sockopt),&socklen);
        ecl_enable_interrupts();

        v5= (ret == 0) ? ecl_make_integer((sockopt.l_onoff != 0) ? sockopt.l_linger : 0) : ECL_NIL;
}
    v4ret = v5;
   }
   if (Null(v4ret)) { goto L2; }
   value0 = v4ret;
   cl_env_copy->nvalues = 1;
   return value0;
L2:;
   T0 = ecl_cstring_to_base_string_or_nil(strerror(errno));
   value0 = cl_error(2, VV[127], T0);
   return value0;
  }
 }
}
/*      function definition for SET-SOCKOPT-INT                       */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L56set_sockopt_int(cl_object v1fd, cl_object v2level, cl_object v3const, cl_object v4value)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v5ret;
   {
    cl_object v6;
    {
        int sockopt = ecl_to_int(v4value);
        int ret;

        ecl_disable_interrupts();
        ret = setsockopt(ecl_to_int(v1fd),ecl_to_int(v2level),ecl_to_int(v3const),wincoerce(char *,&sockopt),sizeof(int));
        ecl_enable_interrupts();

        v6= (ret == 0) ? ECL_T : ECL_NIL;
}
    v5ret = v6;
   }
   if (Null(v5ret)) { goto L2; }
   value0 = v4value;
   cl_env_copy->nvalues = 1;
   return value0;
L2:;
   T0 = ecl_cstring_to_base_string_or_nil(strerror(errno));
   value0 = cl_error(2, VV[127], T0);
   return value0;
  }
 }
}
/*      function definition for SET-SOCKOPT-BOOL                      */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L57set_sockopt_bool(cl_object v1fd, cl_object v2level, cl_object v3const, cl_object v4value)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v5ret;
   {
    cl_object v6;
    {
        int sockopt = (v4value == ECL_NIL) ? 0 : 1;
        int ret;

        ecl_disable_interrupts();
        ret = setsockopt(ecl_to_int(v1fd),ecl_to_int(v2level),ecl_to_int(v3const),wincoerce(char *,&sockopt),sizeof(int));
        ecl_enable_interrupts();

        v6= (ret == 0) ? ECL_T : ECL_NIL;
}
    v5ret = v6;
   }
   if (Null(v5ret)) { goto L2; }
   value0 = v4value;
   cl_env_copy->nvalues = 1;
   return value0;
L2:;
   T0 = ecl_cstring_to_base_string_or_nil(strerror(errno));
   value0 = cl_error(2, VV[127], T0);
   return value0;
  }
 }
}
/*      function definition for SET-SOCKOPT-TIMEVAL                   */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L58set_sockopt_timeval(cl_object v1fd, cl_object v2level, cl_object v3const, cl_object v4value)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v5ret;
   {
    cl_object v6;
    {
        struct timeval tv;
        double tmp = ecl_to_double(v4value);
        int ret;

        ecl_disable_interrupts();
        tv.tv_sec = (int)tmp;
        tv.tv_usec = (int)((tmp-floor(tmp))*1000000.0);
        ret = setsockopt(ecl_to_int(v1fd),ecl_to_int(v2level),ecl_to_int(v3const),&tv,sizeof(struct timeval));
        ecl_enable_interrupts();

        v6= (ret == 0) ? ECL_T : ECL_NIL;
}
    v5ret = v6;
   }
   if (Null(v5ret)) { goto L2; }
   value0 = v4value;
   cl_env_copy->nvalues = 1;
   return value0;
L2:;
   T0 = ecl_cstring_to_base_string_or_nil(strerror(errno));
   value0 = cl_error(2, VV[127], T0);
   return value0;
  }
 }
}
/*      function definition for SET-SOCKOPT-LINGER                    */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L59set_sockopt_linger(cl_object v1fd, cl_object v2level, cl_object v3const, cl_object v4value)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v5ret;
   {
    cl_object v6;
    {
        struct linger sockopt = {0, 0};
        int value = ecl_to_int(v4value);
        int ret;

        if (value > 0) {
                sockopt.l_onoff = 1;
                sockopt.l_linger = value;
        }

        ecl_disable_interrupts();
        ret = setsockopt(ecl_to_int(v1fd),ecl_to_int(v2level),ecl_to_int(v3const),wincoerce(char *,&sockopt),
                         sizeof(struct linger));
        ecl_enable_interrupts();

        v6= (ret == 0) ? ECL_T : ECL_NIL;
}
    v5ret = v6;
   }
   if (Null(v5ret)) { goto L2; }
   value0 = v4value;
   cl_env_copy->nvalues = 1;
   return value0;
L2:;
   T0 = ecl_cstring_to_base_string_or_nil(strerror(errno));
   value0 = cl_error(2, VV[127], T0);
   return value0;
  }
 }
}
/*      local function DEFINE-SOCKOPT                                 */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object LC60define_sockopt(cl_object v1, cl_object v2)
{
 cl_object T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  {
   cl_object v3;
   cl_object v4name;
   cl_object v5c_level;
   cl_object v6c_const;
   cl_object v7type;
   cl_object v8read_only;
   v3 = ecl_cdr(v1);
   if (!(v3==ECL_NIL)) { goto L3; }
   ecl_function_dispatch(cl_env_copy,VV[210])(1, v1) /*  DM-TOO-FEW-ARGUMENTS */;
L3:;
   {
    cl_object v9;
    v9 = ecl_car(v3);
    v3 = ecl_cdr(v3);
    v4name = v9;
   }
   if (!(v3==ECL_NIL)) { goto L9; }
   ecl_function_dispatch(cl_env_copy,VV[210])(1, v1) /*  DM-TOO-FEW-ARGUMENTS */;
L9:;
   {
    cl_object v9;
    v9 = ecl_car(v3);
    v3 = ecl_cdr(v3);
    v5c_level = v9;
   }
   if (!(v3==ECL_NIL)) { goto L15; }
   ecl_function_dispatch(cl_env_copy,VV[210])(1, v1) /*  DM-TOO-FEW-ARGUMENTS */;
L15:;
   {
    cl_object v9;
    v9 = ecl_car(v3);
    v3 = ecl_cdr(v3);
    v6c_const = v9;
   }
   if (!(v3==ECL_NIL)) { goto L21; }
   ecl_function_dispatch(cl_env_copy,VV[210])(1, v1) /*  DM-TOO-FEW-ARGUMENTS */;
L21:;
   {
    cl_object v9;
    v9 = ecl_car(v3);
    v3 = ecl_cdr(v3);
    v7type = v9;
   }
   if (Null(v3)) { goto L27; }
   {
    cl_object v9;
    v9 = ecl_car(v3);
    v3 = ecl_cdr(v3);
    v8read_only = v9;
    goto L26;
   }
L27:;
   v8read_only = ECL_NIL;
L26:;
   if (Null(v3)) { goto L32; }
   ecl_function_dispatch(cl_env_copy,VV[211])(1, v1) /*  DM-TOO-MANY-ARGUMENTS */;
L32:;
   T0 = cl_list(2, ECL_SYM("QUOTE",681), v4name);
   T1 = cl_list(2, ECL_SYM("EXPORT",346), T0);
   T2 = cl_format(3, ECL_NIL, VV[137], v7type);
   T3 = cl_intern(1, T2);
   T4 = cl_list(2, VV[77], v5c_level);
   T5 = cl_list(2, VV[77], v6c_const);
   T6 = cl_list(4, T3, VV[138], T4, T5);
   T7 = cl_list(4, ECL_SYM("DEFUN",291), v4name, VV[136], T6);
   if ((v8read_only)!=ECL_NIL) { goto L35; }
   T9 = cl_list(2, ECL_SYM("SETF",752), v4name);
   T10 = cl_format(3, ECL_NIL, VV[140], v7type);
   T11 = cl_intern(1, T10);
   T12 = cl_list(2, VV[77], v5c_level);
   T13 = cl_list(2, VV[77], v6c_const);
   T14 = cl_list(5, T11, VV[138], T12, T13, VV[141]);
   T15 = cl_list(4, ECL_SYM("DEFUN",291), T9, VV[139], T14);
   T8 = ecl_list1(T15);
   goto L34;
L35:;
   T8 = ECL_NIL;
L34:;
   value0 = cl_listX(4, ECL_SYM("PROGN",673), T1, T7, T8);
   return value0;
  }
 }
}
/*      function definition for SOCKOPT-TYPE                          */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L61sockopt_type(cl_object v1socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v2;
   v2 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v3;
    v3 = (cl_fixnum)(SO_TYPE);
    value0 = L52get_sockopt_int(T0, ecl_make_fixnum(v2), ecl_make_fixnum(v3));
    return value0;
   }
  }
 }
}
/*      function definition for SOCKOPT-RECEIVE-BUFFER                */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L62sockopt_receive_buffer(cl_object v1socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v2;
   v2 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v3;
    v3 = (cl_fixnum)(SO_RCVBUF);
    value0 = L52get_sockopt_int(T0, ecl_make_fixnum(v2), ecl_make_fixnum(v3));
    return value0;
   }
  }
 }
}
/*      function definition for (SETF SOCKOPT-RECEIVE-BUFFER)         */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L63_setf_sockopt_receive_buffer_(cl_object v1value, cl_object v2socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v2socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v3;
   v3 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v4;
    v4 = (cl_fixnum)(SO_RCVBUF);
    value0 = L56set_sockopt_int(T0, ecl_make_fixnum(v3), ecl_make_fixnum(v4), v1value);
    return value0;
   }
  }
 }
}
/*      function definition for SOCKOPT-RECEIVE-TIMEOUT               */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L64sockopt_receive_timeout(cl_object v1socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v2;
   v2 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v3;
    v3 = (cl_fixnum)(SO_RCVTIMEO);
    value0 = L54get_sockopt_timeval(T0, ecl_make_fixnum(v2), ecl_make_fixnum(v3));
    return value0;
   }
  }
 }
}
/*      function definition for (SETF SOCKOPT-RECEIVE-TIMEOUT)        */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L65_setf_sockopt_receive_timeout_(cl_object v1value, cl_object v2socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v2socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v3;
   v3 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v4;
    v4 = (cl_fixnum)(SO_RCVTIMEO);
    value0 = L58set_sockopt_timeval(T0, ecl_make_fixnum(v3), ecl_make_fixnum(v4), v1value);
    return value0;
   }
  }
 }
}
/*      function definition for SOCKOPT-SEND-TIMEOUT                  */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L66sockopt_send_timeout(cl_object v1socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v2;
   v2 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v3;
    v3 = (cl_fixnum)(SO_SNDTIMEO);
    value0 = L54get_sockopt_timeval(T0, ecl_make_fixnum(v2), ecl_make_fixnum(v3));
    return value0;
   }
  }
 }
}
/*      function definition for (SETF SOCKOPT-SEND-TIMEOUT)           */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L67_setf_sockopt_send_timeout_(cl_object v1value, cl_object v2socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v2socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v3;
   v3 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v4;
    v4 = (cl_fixnum)(SO_SNDTIMEO);
    value0 = L58set_sockopt_timeval(T0, ecl_make_fixnum(v3), ecl_make_fixnum(v4), v1value);
    return value0;
   }
  }
 }
}
/*      function definition for SOCKOPT-REUSE-ADDRESS                 */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L68sockopt_reuse_address(cl_object v1socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v2;
   v2 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v3;
    v3 = (cl_fixnum)(SO_REUSEADDR);
    value0 = L53get_sockopt_bool(T0, ecl_make_fixnum(v2), ecl_make_fixnum(v3));
    return value0;
   }
  }
 }
}
/*      function definition for (SETF SOCKOPT-REUSE-ADDRESS)          */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L69_setf_sockopt_reuse_address_(cl_object v1value, cl_object v2socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v2socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v3;
   v3 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v4;
    v4 = (cl_fixnum)(SO_REUSEADDR);
    value0 = L57set_sockopt_bool(T0, ecl_make_fixnum(v3), ecl_make_fixnum(v4), v1value);
    return value0;
   }
  }
 }
}
/*      function definition for SOCKOPT-KEEP-ALIVE                    */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L70sockopt_keep_alive(cl_object v1socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v2;
   v2 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v3;
    v3 = (cl_fixnum)(SO_KEEPALIVE);
    value0 = L53get_sockopt_bool(T0, ecl_make_fixnum(v2), ecl_make_fixnum(v3));
    return value0;
   }
  }
 }
}
/*      function definition for (SETF SOCKOPT-KEEP-ALIVE)             */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L71_setf_sockopt_keep_alive_(cl_object v1value, cl_object v2socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v2socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v3;
   v3 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v4;
    v4 = (cl_fixnum)(SO_KEEPALIVE);
    value0 = L57set_sockopt_bool(T0, ecl_make_fixnum(v3), ecl_make_fixnum(v4), v1value);
    return value0;
   }
  }
 }
}
/*      function definition for SOCKOPT-DONT-ROUTE                    */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L72sockopt_dont_route(cl_object v1socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v2;
   v2 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v3;
    v3 = (cl_fixnum)(SO_DONTROUTE);
    value0 = L53get_sockopt_bool(T0, ecl_make_fixnum(v2), ecl_make_fixnum(v3));
    return value0;
   }
  }
 }
}
/*      function definition for (SETF SOCKOPT-DONT-ROUTE)             */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L73_setf_sockopt_dont_route_(cl_object v1value, cl_object v2socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v2socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v3;
   v3 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v4;
    v4 = (cl_fixnum)(SO_DONTROUTE);
    value0 = L57set_sockopt_bool(T0, ecl_make_fixnum(v3), ecl_make_fixnum(v4), v1value);
    return value0;
   }
  }
 }
}
/*      function definition for SOCKOPT-LINGER                        */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L74sockopt_linger(cl_object v1socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v2;
   v2 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v3;
    v3 = (cl_fixnum)(SO_LINGER);
    value0 = L55get_sockopt_linger(T0, ecl_make_fixnum(v2), ecl_make_fixnum(v3));
    return value0;
   }
  }
 }
}
/*      function definition for (SETF SOCKOPT-LINGER)                 */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L75_setf_sockopt_linger_(cl_object v1value, cl_object v2socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v2socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v3;
   v3 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v4;
    v4 = (cl_fixnum)(SO_LINGER);
    value0 = L59set_sockopt_linger(T0, ecl_make_fixnum(v3), ecl_make_fixnum(v4), v1value);
    return value0;
   }
  }
 }
}
/*      function definition for SOCKOPT-REUSE-PORT                    */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L76sockopt_reuse_port(cl_object v1socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v2;
   v2 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v3;
    v3 = (cl_fixnum)(SO_REUSEPORT);
    value0 = L53get_sockopt_bool(T0, ecl_make_fixnum(v2), ecl_make_fixnum(v3));
    return value0;
   }
  }
 }
}
/*      function definition for (SETF SOCKOPT-REUSE-PORT)             */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L77_setf_sockopt_reuse_port_(cl_object v1value, cl_object v2socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v2socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v3;
   v3 = (cl_fixnum)(SOL_SOCKET);
   {
    cl_fixnum v4;
    v4 = (cl_fixnum)(SO_REUSEPORT);
    value0 = L57set_sockopt_bool(T0, ecl_make_fixnum(v3), ecl_make_fixnum(v4), v1value);
    return value0;
   }
  }
 }
}
/*      function definition for SOCKOPT-TCP-NODELAY                   */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L78sockopt_tcp_nodelay(cl_object v1socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v1socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v2;
   v2 = (cl_fixnum)(IPPROTO_TCP);
   {
    cl_fixnum v3;
    v3 = (cl_fixnum)(TCP_NODELAY);
    value0 = L53get_sockopt_bool(T0, ecl_make_fixnum(v2), ecl_make_fixnum(v3));
    return value0;
   }
  }
 }
}
/*      function definition for (SETF SOCKOPT-TCP-NODELAY)            */
/*      optimize speed 1, debug 1, space 1, safety 2                  */
static cl_object L79_setf_sockopt_tcp_nodelay_(cl_object v1value, cl_object v2socket)
{
 cl_object T0;
 cl_object env0 = ECL_NIL;
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 ecl_cs_check(cl_env_copy,value0);
 {
TTL:
  T0 = ecl_function_dispatch(cl_env_copy,VV[177])(1, v2socket) /*  SOCKET-FILE-DESCRIPTOR */;
  {
   cl_fixnum v3;
   v3 = (cl_fixnum)(IPPROTO_TCP);
   {
    cl_fixnum v4;
    v4 = (cl_fixnum)(TCP_NODELAY);
    value0 = L57set_sockopt_bool(T0, ecl_make_fixnum(v3), ecl_make_fixnum(v4), v1value);
    return value0;
   }
  }
 }
}

#include "ext/sockets.data"
#ifdef __cplusplus
extern "C"
#endif
ECL_DLLEXPORT void _eclekPx67Hqtmli9_IS5L6z51(cl_object flag)
{
 const cl_env_ptr cl_env_copy = ecl_process_env();
 cl_object value0;
 cl_object *VVtemp;
 if (flag != OBJNULL){
 Cblock = flag;
 #ifndef ECL_DYNAMIC_VV
 flag->cblock.data = VV;
 #endif
 flag->cblock.data_size = VM;
 flag->cblock.temp_data_size = VMtemp;
 flag->cblock.data_text = compiler_data_text;
 flag->cblock.cfuns_size = compiler_cfuns_size;
 flag->cblock.cfuns = compiler_cfuns;
 flag->cblock.source = ecl_make_constant_base_string("EXT:SOCKETS;SOCKETS.LISP.NEWEST",-1);
 return;}
 #ifdef ECL_DYNAMIC_VV
 VV = Cblock->cblock.data;
 #endif
 Cblock->cblock.data_text = (const cl_object *)"@EcLtAg:_eclekPx67Hqtmli9_IS5L6z51@";
 VVtemp = Cblock->cblock.temp_data;
 ECL_DEFINE_SETF_FUNCTIONS
  si_select_package(VVtemp[0]);
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(AF_INET);
   si_Xmake_constant(VV[0], ecl_make_fixnum(v1));
  }
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(AF_LOCAL);
   si_Xmake_constant(VV[1], ecl_make_fixnum(v1));
  }
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EAGAIN);
   si_Xmake_constant(VV[2], ecl_make_fixnum(v1));
  }
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EINTR);
   si_Xmake_constant(VV[3], ecl_make_fixnum(v1));
  }
  ecl_cmp_defun(VV[164]);                         /*  FF-SOCKET       */
  ecl_cmp_defun(VV[165]);                         /*  FF-LISTEN       */
  ecl_cmp_defun(VV[166]);                         /*  FF-CLOSE        */
  ecl_cmp_defun(VV[167]);                         /*  SPLIT           */
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  T0 = cl_list(2, ECL_SYM("DOCUMENTATION",1244), VVtemp[2]);
  clos_load_defclass(VV[9], ECL_NIL, VVtemp[1], T0);
 }
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[10], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[3], ECL_SYM("DOCUMENTATION",1244), VVtemp[2]) /*  ENSURE-GENERIC-FUNCTION */;
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC6host_ent_address,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[10], ECL_NIL, VVtemp[3], VVtemp[3], T0) /*  INSTALL-METHOD */;
 }
  ecl_cmp_defun(VV[173]);                         /*  GET-HOST-BY-NAME */
  ecl_cmp_defun(VV[174]);                         /*  GET-HOST-BY-ADDRESS */
 {
  cl_object T0, T1, T2, T3;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC9__lambda31,ECL_NIL,Cblock,0);
   T0 = v1;
  }
  T1 = cl_listX(3, ECL_SYM("INITFUNCTION",999), T0, VVtemp[5]);
  T2 = cl_list(5, VVtemp[4], T1, VVtemp[6], VVtemp[7], VVtemp[8]);
  T3 = cl_list(2, ECL_SYM("DOCUMENTATION",1244), VVtemp[9]);
  clos_load_defclass(VV[19], ECL_NIL, T2, T3);
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC11print_object,ECL_NIL,Cblock,2);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, ECL_SYM("PRINT-OBJECT",963), ECL_NIL, VVtemp[10], VVtemp[11], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun_va((cl_objectfn)LC12shared_initialize,ECL_NIL,Cblock,2);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, ECL_SYM("SHARED-INITIALIZE",967), VVtemp[12], VVtemp[10], VVtemp[13], T0) /*  INSTALL-METHOD */;
 }
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[30], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[14], ECL_SYM("DOCUMENTATION",1244), VVtemp[15]) /*  ENSURE-GENERIC-FUNCTION */;
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[31], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[16], ECL_SYM("DOCUMENTATION",1244), VVtemp[17]) /*  ENSURE-GENERIC-FUNCTION */;
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[32], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[14], ECL_SYM("DOCUMENTATION",1244), VVtemp[18]) /*  ENSURE-GENERIC-FUNCTION */;
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[33], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[16], ECL_SYM("DOCUMENTATION",1244), VVtemp[19]) /*  ENSURE-GENERIC-FUNCTION */;
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[34], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[16], ECL_SYM("DOCUMENTATION",1244), VVtemp[20]) /*  ENSURE-GENERIC-FUNCTION */;
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[35], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[21], ECL_SYM("DOCUMENTATION",1244), VVtemp[22]) /*  ENSURE-GENERIC-FUNCTION */;
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[36], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[23], ECL_SYM("DOCUMENTATION",1244), VVtemp[24]) /*  ENSURE-GENERIC-FUNCTION */;
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[37], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[25], ECL_SYM("DOCUMENTATION",1244), VVtemp[26]) /*  ENSURE-GENERIC-FUNCTION */;
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[38], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[27], ECL_SYM("DOCUMENTATION",1244), VVtemp[28]) /*  ENSURE-GENERIC-FUNCTION */;
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[39], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[29], ECL_SYM("DOCUMENTATION",1244), VVtemp[30]) /*  ENSURE-GENERIC-FUNCTION */;
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[40], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[16], ECL_SYM("DOCUMENTATION",1244), VVtemp[31]) /*  ENSURE-GENERIC-FUNCTION */;
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VVtemp[32], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[33], ECL_SYM("DOCUMENTATION",1244), VVtemp[34]) /*  ENSURE-GENERIC-FUNCTION */;
  (cl_env_copy->function=(ECL_SYM("ENSURE-GENERIC-FUNCTION",944)->symbol.gfdef))->cfun.entry(7, VV[41], VV[11], ECL_T, ECL_SYM("LAMBDA-LIST",1000), VVtemp[16], ECL_SYM("DOCUMENTATION",1244), VVtemp[35]) /*  ENSURE-GENERIC-FUNCTION */;
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC13socket_listen,ECL_NIL,Cblock,2);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[35], ECL_NIL, VVtemp[10], VVtemp[21], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC14socket_close_low_level,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[41], ECL_NIL, VVtemp[16], VVtemp[16], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun_va((cl_objectfn)LC15socket_close,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[38], ECL_NIL, VVtemp[16], VVtemp[27], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun_va((cl_objectfn)LC16socket_receive,ECL_NIL,Cblock,3);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[36], ECL_NIL, VVtemp[36], VVtemp[37], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0, T1, T2;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC18__lambda69,ECL_NIL,Cblock,0);
   T0 = v1;
  }
  T1 = cl_listX(3, ECL_SYM("INITFUNCTION",999), T0, VVtemp[39]);
  T2 = cl_list(2, T1, VVtemp[40]);
  clos_load_defclass(VV[46], VVtemp[38], T2, ECL_NIL);
 }
  ecl_cmp_defun(VV[187]);                         /*  GET-PROTOCOL-BY-NAME */
  ecl_cmp_defun(VV[188]);                         /*  MAKE-INET-ADDRESS */
 {
  cl_object T0, T1, T2, T3;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC21__lambda72,ECL_NIL,Cblock,0);
   T0 = v1;
  }
  T1 = cl_listX(3, ECL_SYM("INITFUNCTION",999), T0, VVtemp[41]);
  T2 = ecl_list1(T1);
  T3 = cl_list(2, ECL_SYM("DOCUMENTATION",1244), VVtemp[42]);
  clos_load_defclass(VV[51], VVtemp[16], T2, T3);
 }
  ecl_cmp_defun(VV[189]);                         /*  MAKE-INET-SOCKET */
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun_va((cl_objectfn)LC23socket_bind,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[30], ECL_NIL, VVtemp[43], VVtemp[14], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC24socket_accept,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[31], ECL_NIL, VVtemp[43], VVtemp[16], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun_va((cl_objectfn)LC25socket_connect,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[32], ECL_NIL, VVtemp[43], VVtemp[14], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC26socket_peername,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[33], ECL_NIL, VVtemp[43], VVtemp[16], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC27socket_name,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[34], ECL_NIL, VVtemp[43], VVtemp[16], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun_va((cl_objectfn)LC28socket_send,ECL_NIL,Cblock,3);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[37], ECL_NIL, VVtemp[36], VVtemp[25], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0, T1, T2, T3;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC29__lambda88,ECL_NIL,Cblock,0);
   T0 = v1;
  }
  T1 = cl_listX(3, ECL_SYM("INITFUNCTION",999), T0, VVtemp[44]);
  T2 = ecl_list1(T1);
  T3 = cl_list(2, ECL_SYM("DOCUMENTATION",1244), VVtemp[45]);
  clos_load_defclass(VV[65], VVtemp[16], T2, T3);
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun_va((cl_objectfn)LC30socket_bind,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[30], ECL_NIL, VVtemp[46], VVtemp[14], T0) /*  INSTALL-METHOD */;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC31socket_accept,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[31], ECL_NIL, VVtemp[46], VVtemp[16], T0) /*  INSTALL-METHOD */;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun_va((cl_objectfn)LC32socket_connect,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[32], ECL_NIL, VVtemp[46], VVtemp[14], T0) /*  INSTALL-METHOD */;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC33socket_peername,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[33], ECL_NIL, VVtemp[46], VVtemp[16], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC34non_blocking_mode,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[40], ECL_NIL, VVtemp[16], VVtemp[16], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC35_setf_non_blocking_mode_,ECL_NIL,Cblock,2);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VVtemp[32], ECL_NIL, VVtemp[47], VVtemp[33], T0) /*  INSTALL-METHOD */;
 }
  ecl_cmp_defun(VV[200]);                         /*  DUP             */
  (void)0; /* No entry created for AUTO-CLOSE-TWO-WAY-STREAM */
  ecl_cmp_defun(VV[201]);                         /*  SOCKET-MAKE-STREAM-INNER */
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun_va((cl_objectfn)LC39socket_make_stream,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[39], ECL_NIL, VVtemp[16], VVtemp[48], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC40ext__stream_fd,ECL_NIL,Cblock,1);
   T0 = v1;
  }
  ecl_function_dispatch(cl_env_copy,VV[171])(5, VV[74], ECL_NIL, VVtemp[16], VVtemp[16], T0) /*  INSTALL-METHOD */;
 }
 {
  cl_object T0, T1, T2, T3;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC42__lambda127,ECL_NIL,Cblock,0);
   T0 = v1;
  }
  T1 = cl_listX(3, ECL_SYM("INITFUNCTION",999), T0, VVtemp[49]);
  T2 = cl_list(4, T1, VVtemp[50], VVtemp[51], VVtemp[52]);
  T3 = cl_list(2, ECL_SYM("DOCUMENTATION",1244), VVtemp[53]);
  clos_load_defclass(VV[75], VVtemp[38], T2, T3);
 }
  ecl_cmp_defmacro(VV[209]);                      /*  DEFINE-SOCKET-CONDITION */
  si_Xmake_special(VV[80]);
  cl_set(VV[80],ECL_NIL);
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EADDRINUSE);
   si_Xmake_constant(VV[82], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[83], VV[78], VVtemp[54], ECL_NIL);
  cl_export(1, VV[83]);
  T0 = CONS(ecl_symbol_value(VV[82]),VV[83]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EAGAIN);
   si_Xmake_constant(VV[84], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[85], VV[78], VVtemp[55], ECL_NIL);
  cl_export(1, VV[85]);
  T0 = CONS(ecl_symbol_value(VV[84]),VV[85]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EBADF);
   si_Xmake_constant(VV[86], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[87], VV[78], VVtemp[56], ECL_NIL);
  cl_export(1, VV[87]);
  T0 = CONS(ecl_symbol_value(VV[86]),VV[87]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(ECONNREFUSED);
   si_Xmake_constant(VV[88], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[89], VV[78], VVtemp[57], ECL_NIL);
  cl_export(1, VV[89]);
  T0 = CONS(ecl_symbol_value(VV[88]),VV[89]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(ETIMEDOUT);
   si_Xmake_constant(VV[90], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[91], VV[78], VVtemp[58], ECL_NIL);
  cl_export(1, VV[91]);
  T0 = CONS(ecl_symbol_value(VV[90]),VV[91]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EINTR);
   si_Xmake_constant(VV[92], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[85], VV[78], VVtemp[59], ECL_NIL);
  cl_export(1, VV[85]);
  T0 = CONS(ecl_symbol_value(VV[92]),VV[85]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EINVAL);
   si_Xmake_constant(VV[93], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[94], VV[78], VVtemp[60], ECL_NIL);
  cl_export(1, VV[94]);
  T0 = CONS(ecl_symbol_value(VV[93]),VV[94]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(ENOBUFS);
   si_Xmake_constant(VV[95], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[96], VV[78], VVtemp[61], ECL_NIL);
  cl_export(1, VV[96]);
  T0 = CONS(ecl_symbol_value(VV[95]),VV[96]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(ENOMEM);
   si_Xmake_constant(VV[97], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[98], VV[78], VVtemp[62], ECL_NIL);
  cl_export(1, VV[98]);
  T0 = CONS(ecl_symbol_value(VV[97]),VV[98]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EOPNOTSUPP);
   si_Xmake_constant(VV[99], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[100], VV[78], VVtemp[63], ECL_NIL);
  cl_export(1, VV[100]);
  T0 = CONS(ecl_symbol_value(VV[99]),VV[100]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EPERM);
   si_Xmake_constant(VV[101], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[102], VV[78], VVtemp[64], ECL_NIL);
  cl_export(1, VV[102]);
  T0 = CONS(ecl_symbol_value(VV[101]),VV[102]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EPROTONOSUPPORT);
   si_Xmake_constant(VV[103], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[104], VV[78], VVtemp[65], ECL_NIL);
  cl_export(1, VV[104]);
  T0 = CONS(ecl_symbol_value(VV[103]),VV[104]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(ESOCKTNOSUPPORT);
   si_Xmake_constant(VV[105], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[106], VV[78], VVtemp[66], ECL_NIL);
  cl_export(1, VV[106]);
  T0 = CONS(ecl_symbol_value(VV[105]),VV[106]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(ENETUNREACH);
   si_Xmake_constant(VV[107], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[108], VV[78], VVtemp[67], ECL_NIL);
  cl_export(1, VV[108]);
  T0 = CONS(ecl_symbol_value(VV[107]),VV[108]);
  cl_set(VV[80],CONS(T0,ecl_symbol_value(VV[80])));
 }
  ecl_cmp_defun(VV[212]);                         /*  CONDITION-FOR-ERRNO */
  ecl_cmp_defun(VV[213]);                         /*  SOCKET-ERROR    */
  ecl_cmp_defun(VV[214]);                         /*  NAME-SERVICE-ERROR */
 {
  cl_object T0, T1, T2;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_object volatile v1;
   v1 = ecl_make_cfun((cl_objectfn_fixed)LC48__lambda151,ECL_NIL,Cblock,0);
   T0 = v1;
  }
  T1 = cl_listX(3, ECL_SYM("INITFUNCTION",999), T0, VVtemp[69]);
  T2 = cl_list(4, T1, VVtemp[70], VVtemp[71], VVtemp[72]);
  clos_load_defclass(VV[112], VVtemp[68], T2, ECL_NIL);
 }
  ecl_cmp_defmacro(VV[217]);                      /*  DEFINE-NAME-SERVICE-CONDITION */
  si_Xmake_special(VV[116]);
  cl_set(VV[116],ECL_NIL);
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EAI_NONAME);
   si_Xmake_constant(VV[118], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[119], VV[114], VVtemp[73], ECL_NIL);
  T0 = CONS(ecl_symbol_value(VV[118]),VV[119]);
  cl_set(VV[116],CONS(T0,ecl_symbol_value(VV[116])));
  cl_export(1, VV[118]);
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EAI_AGAIN);
   si_Xmake_constant(VV[120], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[121], VV[114], VVtemp[74], ECL_NIL);
  T0 = CONS(ecl_symbol_value(VV[120]),VV[121]);
  cl_set(VV[116],CONS(T0,ecl_symbol_value(VV[116])));
  cl_export(1, VV[120]);
 }
 {
  cl_object T0;
  cl_object volatile env0 = ECL_NIL;
  {
   cl_fixnum v1;
   v1 = (cl_fixnum)(EAI_FAIL);
   si_Xmake_constant(VV[122], ecl_make_fixnum(v1));
  }
  clos_load_defclass(VV[123], VV[114], VVtemp[75], ECL_NIL);
  T0 = CONS(ecl_symbol_value(VV[122]),VV[123]);
  cl_set(VV[116],CONS(T0,ecl_symbol_value(VV[116])));
  cl_export(1, VV[122]);
 }
  ecl_cmp_defun(VV[218]);                         /*  CONDITION-FOR-NAME-SERVICE-ERRNO */
  ecl_cmp_defun(VV[219]);                         /*  GET-NAME-SERVICE-ERROR-MESSAGE */
  ecl_cmp_defun(VV[220]);                         /*  GET-SOCKOPT-INT */
  ecl_cmp_defun(VV[221]);                         /*  GET-SOCKOPT-BOOL */
  ecl_cmp_defun(VV[222]);                         /*  GET-SOCKOPT-TIMEVAL */
  ecl_cmp_defun(VV[223]);                         /*  GET-SOCKOPT-LINGER */
  ecl_cmp_defun(VV[224]);                         /*  SET-SOCKOPT-INT */
  ecl_cmp_defun(VV[225]);                         /*  SET-SOCKOPT-BOOL */
  ecl_cmp_defun(VV[226]);                         /*  SET-SOCKOPT-TIMEVAL */
  ecl_cmp_defun(VV[227]);                         /*  SET-SOCKOPT-LINGER */
  ecl_cmp_defmacro(VV[228]);                      /*  DEFINE-SOCKOPT  */
  cl_export(1, VV[143]);
  ecl_cmp_defun(VV[229]);                         /*  SOCKOPT-TYPE    */
  cl_export(1, VV[144]);
  ecl_cmp_defun(VV[230]);                         /*  SOCKOPT-RECEIVE-BUFFER */
  ecl_cmp_defun(VV[231]);                         /*  (SETF SOCKOPT-RECEIVE-BUFFER) */
  cl_export(1, VV[146]);
  ecl_cmp_defun(VV[232]);                         /*  SOCKOPT-RECEIVE-TIMEOUT */
  ecl_cmp_defun(VV[233]);                         /*  (SETF SOCKOPT-RECEIVE-TIMEOUT) */
  cl_export(1, VV[148]);
  ecl_cmp_defun(VV[234]);                         /*  SOCKOPT-SEND-TIMEOUT */
  ecl_cmp_defun(VV[235]);                         /*  (SETF SOCKOPT-SEND-TIMEOUT) */
  cl_export(1, VV[150]);
  ecl_cmp_defun(VV[236]);                         /*  SOCKOPT-REUSE-ADDRESS */
  ecl_cmp_defun(VV[237]);                         /*  (SETF SOCKOPT-REUSE-ADDRESS) */
  cl_export(1, VV[152]);
  ecl_cmp_defun(VV[238]);                         /*  SOCKOPT-KEEP-ALIVE */
  ecl_cmp_defun(VV[239]);                         /*  (SETF SOCKOPT-KEEP-ALIVE) */
  cl_export(1, VV[154]);
  ecl_cmp_defun(VV[240]);                         /*  SOCKOPT-DONT-ROUTE */
  ecl_cmp_defun(VV[241]);                         /*  (SETF SOCKOPT-DONT-ROUTE) */
  cl_export(1, VV[156]);
  ecl_cmp_defun(VV[242]);                         /*  SOCKOPT-LINGER  */
  ecl_cmp_defun(VV[243]);                         /*  (SETF SOCKOPT-LINGER) */
  cl_export(1, VV[158]);
  ecl_cmp_defun(VV[244]);                         /*  SOCKOPT-REUSE-PORT */
  ecl_cmp_defun(VV[245]);                         /*  (SETF SOCKOPT-REUSE-PORT) */
  cl_export(1, VV[160]);
  ecl_cmp_defun(VV[246]);                         /*  SOCKOPT-TCP-NODELAY */
  ecl_cmp_defun(VV[247]);                         /*  (SETF SOCKOPT-TCP-NODELAY) */
  cl_provide(VV[162]);
  cl_provide(VV[163]);
}
